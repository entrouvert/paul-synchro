# Django default settings for passerelle project.

from django.conf import global_settings
import os
import logging

try:
    from logging.handlers import NullHandler
except ImportError:
    # python < 2.7
    class NullHandler(logging.Handler):
        def emit(self, record):
            pass

logging.getLogger('passerelle').addHandler(NullHandler())

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

### Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/dev/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'please-change-me-with-a-very-long-random-string'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
TEMPLATE_DEBUG = True

# See https://docs.djangoproject.com/en/dev/ref/settings/#allowed-hosts
ALLOWED_HOSTS = []

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'passerelle.sqlite3',
    }
}

### End of "Quick-start development settings"


# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (os.path.join(BASE_DIR, 'passerelle', 'static'),)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = global_settings.STATICFILES_FINDERS + ('gadjo.finders.XStaticFinder',)

MIDDLEWARE_CLASSES = global_settings.MIDDLEWARE_CLASSES + (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'passerelle.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'passerelle.wsgi.application'

LOCALE_PATHS = (os.path.join(BASE_DIR, 'passerelle', 'locale'),)
LANGUAGE_CODE = 'fr-fr'

TEMPLATE_DIRS = (os.path.join(BASE_DIR, 'passerelle', 'templates'),)

TEMPLATE_CONTEXT_PROCESSORS = global_settings.TEMPLATE_CONTEXT_PROCESSORS + (
    'django.core.context_processors.request',
    'passerelle.base.context_processors.template_vars',
)

INSTALLED_APPS = (
    # system apps
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    # base apps
    'passerelle.base',
    'passerelle.datasources',
    'passerelle.repost',
    # connectors
    'clicrdv',
    'gdc',
    'choosit',
    'oxyd',
    'ovh',
    'mobyt',
    'pastell',
    'bdp',
    'base_adresse',
    'csvdatasource',
    'ldap',
    'orange',
    'family',
    # backoffice templates and static
    'gadjo',
)

# active applications
PASSERELLE_APP_CLICRDV_ENABLED = True
PASSERELLE_APP_BASE_ADRESSE_ENABLED = True
PASSERELLE_APP_CSVDATASOURCE_ENABLED = True
PASSERELLE_APP_LDAP_ENABLED = True
PASSERELLE_APP_CHOOSIT_ENABLED = True
PASSERELLE_APP_OXYD_ENABLED = True
PASSERELLE_APP_OVH_ENABLED = True
PASSERELLE_APP_MOBYT_ENABLED = True
PASSERELLE_APP_ORANGE_ENABLED = True
PASSERELLE_APP_FAMILY_ENABLED = True

PASSERELLE_APP_BDP_ENABLED = False
PASSERELLE_APP_GDC_ENABLED = False
PASSERELLE_APP_PASTELL_ENABLED = False
PASSERELLE_APP_CONCERTO_ENABLED = False

# Authentication settings
try:
    import mellon
except ImportError:
    mellon = None

if mellon is not None:
    AUTHENTICATION_BACKENDS = (
        'mellon.backends.SAMLBackend',
        'django.contrib.auth.backends.ModelBackend',
    )

LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/'
LOGOUT_URL = '/logout/'

MELLON_ATTRIBUTE_MAPPING = {
    'email': '{attributes[email][0]}',
    'first_name': '{attributes[first_name][0]}',
    'last_name': '{attributes[last_name][0]}',
}

MELLON_SUPERUSER_MAPPING = {
    'is_superuser': 'true',
}

MELLON_USERNAME_TEMPLATE = '{attributes[name_id_content]}'

MELLON_IDENTITY_PROVIDERS = []

# REQUESTS_PROXIES that can be used by requests methods
# see http://docs.python-requests.org/en/latest/user/advanced/#proxies
REQUESTS_PROXIES = {}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            },
    },
    'loggers': {
        'django.request': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'DEBUG'),
            'propragate': True,
        },
        'passerelle.resource': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'DEBUG'),
            'propragate': True,
        },
    },
}

local_settings_file = os.environ.get('PASSERELLE_SETTINGS_FILE',
        os.path.join(os.path.dirname(__file__), 'local_settings.py'))
if os.path.exists(local_settings_file):
    execfile(local_settings_file)
