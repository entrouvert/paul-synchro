import sys
import os.path

apps_dir = os.path.join(os.path.dirname(__file__), 'apps')
if apps_dir not in sys.path:
    sys.path.append(apps_dir)
