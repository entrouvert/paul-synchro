from django.contrib import admin

from .models import JSONOutputQueue, MakoPrototypeOutputQueue, MakoOutputQueue

class BaseQueueAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(JSONOutputQueue, BaseQueueAdmin)
admin.site.register(MakoPrototypeOutputQueue, BaseQueueAdmin)
admin.site.register(MakoOutputQueue, BaseQueueAdmin)
