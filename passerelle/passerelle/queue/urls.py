from django.conf import settings
from django.conf.urls import patterns, url

from ..base.views import ResourceIndexView, ResourceView
from .models import BaseQueue

urlpatterns = patterns('passerelle.queue.views',
    # human views
    url(r'^$', ResourceIndexView.as_view(model=BaseQueue)),
    url(r'^(?P<slug>[\w-]+)/$', ResourceView.as_view(model=BaseQueue,
        template_name='queue/view.html')),
    # technical views (JSON)
    url(r'^(?P<slug>[\w-]+)/json$', 'queue'),
)
