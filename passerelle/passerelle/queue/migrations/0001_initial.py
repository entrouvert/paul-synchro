# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BaseQueue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50)),
                ('slug', models.SlugField()),
                ('description', models.TextField()),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='JSONOutputQueue',
            fields=[
                ('basequeue_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='queue.BaseQueue')),
                ('directory', models.CharField(help_text=b'created files will have filename like 2013-10-03T03:34:34-c009433e-2d02-11e3-a9d2-843a4b96a544 where first is the date and time of arrival and the second part is an UUID', max_length=200, verbose_name='directory where the data will be stored')),
                ('tmp_directory', models.CharField(default=b'/var/tmp', help_text='must be on the same file-system as directory to guarantee atomicity of rename', max_length=200, verbose_name='directory where temporary files are created')),
                ('output_encoding', models.CharField(default=b'utf-8', max_length=32)),
            ],
            options={
                'abstract': False,
            },
            bases=('queue.basequeue',),
        ),
        migrations.CreateModel(
            name='MakoOutputQueue',
            fields=[
                ('basequeue_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='queue.BaseQueue')),
                ('directory', models.CharField(help_text=b'created files will have filename like 2013-10-03T03:34:34-c009433e-2d02-11e3-a9d2-843a4b96a544 where first is the date and time of arrival and the second part is an UUID', max_length=200, verbose_name='directory where the data will be stored')),
                ('tmp_directory', models.CharField(default=b'/var/tmp', help_text='must be on the same file-system as directory to guarantee atomicity of rename', max_length=200, verbose_name='directory where temporary files are created')),
                ('output_encoding', models.CharField(default=b'utf-8', max_length=32)),
            ],
            options={
                'abstract': False,
            },
            bases=('queue.basequeue',),
        ),
        migrations.CreateModel(
            name='MakoPrototypeOutputQueue',
            fields=[
                ('basequeue_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='queue.BaseQueue')),
                ('directory', models.CharField(help_text=b'created files will have filename like 2013-10-03T03:34:34-c009433e-2d02-11e3-a9d2-843a4b96a544 where first is the date and time of arrival and the second part is an UUID', max_length=200, verbose_name='directory where the data will be stored')),
                ('tmp_directory', models.CharField(default=b'/var/tmp', help_text='must be on the same file-system as directory to guarantee atomicity of rename', max_length=200, verbose_name='directory where temporary files are created')),
                ('output_encoding', models.CharField(default=b'utf-8', max_length=32)),
                ('template', models.TextField(verbose_name='inline mako template for formatting the output')),
            ],
            options={
                'abstract': False,
            },
            bases=('queue.basequeue',),
        ),
        migrations.AddField(
            model_name='basequeue',
            name='users',
            field=models.ManyToManyField(to='base.ApiUser', blank=True),
            preserve_default=True,
        ),
    ]
