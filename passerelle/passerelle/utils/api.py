# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2016 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


class endpoint(object):
    def __init__(self, serializer_type='json', perm=None, methods=['get'], name=None, pattern=None,
                 wrap_response=True):
        self.perm = perm
        self.methods = methods
        self.serializer_type = serializer_type
        self.pattern = pattern
        self.name = name
        self.wrap_response = wrap_response

    def __call__(self, func):
        func.endpoint_info = self
        if not self.name:
            self.name = func.func_name
        return func
