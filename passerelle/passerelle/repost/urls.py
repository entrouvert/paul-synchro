from django.conf.urls import patterns, url

from ..base.views import ResourceIndexView, ResourceView
from models import BaseRepost

urlpatterns = patterns('passerelle.repost.views',
    # human views
    url(r'^$', ResourceIndexView.as_view(model=BaseRepost)),
    url(r'^(?P<slug>[\w-]+)/$', ResourceView.as_view(model=BaseRepost,
        template_name='repost/view.html')),
    # technical views (JSON)
    url(r'^(?P<slug>[\w-]+)/json$', 'repost'),
)
