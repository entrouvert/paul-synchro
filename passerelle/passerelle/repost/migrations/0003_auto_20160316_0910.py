# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('repost', '0002_baserepost_log_level'),
    ]

    operations = [
        migrations.AlterField(
            model_name='baserepost',
            name='log_level',
            field=models.CharField(default=b'NOTSET', max_length=10, verbose_name='Log Level', choices=[(b'DEBUG', b'DEBUG'), (b'INFO', b'INFO')]),
            preserve_default=True,
        ),
    ]
