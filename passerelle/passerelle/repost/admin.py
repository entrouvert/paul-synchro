from django.contrib import admin

from .models import BaseRepost

class BaseRepostAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(BaseRepost, BaseRepostAdmin)
