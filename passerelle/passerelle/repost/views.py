import json

from django.views.decorators.csrf import csrf_exempt
from passerelle.utils import to_json

from models import BaseRepost

@csrf_exempt
@to_json('api')
def repost(request, slug, **kwargs):
    rp = BaseRepost.objects.get_slug(slug, request)
    if request.method == 'POST':
        data = json.loads(request.body)
    else:
        data = None
    if rp.parameters == '*':
        kwargs = request.GET.copy()
    else:
        kwargs = {}
        for param in (rp.parameters or ()):
            kwargs[param] = request.GET.get(param)
    return rp.repost(data, **kwargs)
