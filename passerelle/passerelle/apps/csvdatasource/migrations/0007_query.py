# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('csvdatasource', '0006_auto_20160905_1031'),
    ]

    operations = [
        migrations.CreateModel(
            name='Query',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', models.SlugField(verbose_name='Name')),
                ('filters', models.TextField(help_text='List of filter clauses (Python expression)', verbose_name='Filters', blank=True)),
                ('projections', models.TextField(help_text='List of projections (name:expression)', verbose_name='Projections', blank=True)),
                ('order', models.TextField(help_text='Ordering columns', verbose_name='Order', blank=True)),
                ('distinct', models.TextField(help_text='Distinct columns', verbose_name='Distinct', blank=True)),
                ('structure', models.CharField(default=b'dict', help_text='Data structure used for the response', max_length=20, verbose_name='Structure', choices=[(b'array', 'Array'), (b'dict', 'Dictionary'), (b'tuples', 'Tuples'), (b'onerow', 'Single Row'), (b'one', 'Single Value')])),
                ('resource', models.ForeignKey(to='csvdatasource.CsvDataSource')),
            ],
            options={
                'ordering': ['slug'],
            },
        ),
    ]
