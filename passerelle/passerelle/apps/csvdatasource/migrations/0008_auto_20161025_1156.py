# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('csvdatasource', '0007_query'),
    ]

    operations = [
        migrations.AddField(
            model_name='query',
            name='description',
            field=models.TextField(verbose_name='Description', blank=True),
        ),
        migrations.AddField(
            model_name='query',
            name='label',
            field=models.CharField(default='', max_length=100, verbose_name='Label'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='query',
            name='slug',
            field=models.SlugField(verbose_name='Name (slug)'),
        ),
    ]
