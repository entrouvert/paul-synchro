# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('csvdatasource', '0004_auto_20160407_0456'),
    ]

    operations = [
        migrations.AddField(
            model_name='csvdatasource',
            name='_dialect_options',
            field=jsonfield.fields.JSONField(default=dict, editable=False),
            preserve_default=True,
        ),
    ]
