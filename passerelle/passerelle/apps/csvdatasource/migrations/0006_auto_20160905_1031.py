# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('csvdatasource', '0005_csvdatasource__dialect_options'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='csvdatasource',
            name='_dialect_options',
        ),
        migrations.AddField(
            model_name='csvdatasource',
            name='_dialect_options',
            field=jsonfield.fields.JSONField(null=True, editable=False),
        ),
    ]
