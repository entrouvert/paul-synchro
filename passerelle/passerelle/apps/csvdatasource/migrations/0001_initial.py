# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CsvDataSource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50)),
                ('slug', models.SlugField()),
                ('description', models.TextField()),
                ('csv_file', models.FileField(upload_to=b'csv', verbose_name='CSV File')),
                ('columns_keynames', models.CharField(default=b'id, text',
                                                      help_text='ex: id,text,data1,data2',
                                                      max_length=256,
                                                      verbose_name='Column keynames',
                                                      blank=True)),
                ('skip_header', models.BooleanField(default=False, verbose_name='Skip first line')),
                ('users', models.ManyToManyField(to='base.ApiUser', blank=True)),
            ],
            options={
                'verbose_name': 'CSV File',
            },
            bases=(models.Model,),
        ),
    ]
