# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.contrib.contenttypes.management import update_contenttypes

def migrate_clicrdv(apps, schema_editor):
    app_config = apps.app_configs['clicrdv']
    app_config.models_module = app_config.models_module or True
    update_contenttypes(app_config)
    ClicRdv = apps.get_model('clicrdv', 'ClicRdv')
    NewClicRdv = apps.get_model('clicrdv', 'NewClicRdv')
    AccessRight = apps.get_model('base', 'AccessRight')
    ContentType = apps.get_model('contenttypes', 'ContentType')
    old_ctype_id = apps.get_model('contenttypes', 'ContentType').objects.get(app_label='clicrdv', model='clicrdv').id
    new_ctype_id = apps.get_model('contenttypes', 'ContentType').objects.get(app_label='clicrdv', model='newclicrdv').id
    for old in ClicRdv.objects.all():
        new_dict = dict([x for x in old.__dict__.items() if x[0] not in ( 'id', 'basedatasource_ptr_id', '_state')])
        new_object = NewClicRdv(**new_dict)
        new_object.save()
        for access_right in AccessRight.objects.filter(resource_type_id=old_ctype_id, resource_pk=old.basedatasource_ptr_id):
            access_right.resource_pk = new_object.id
            access_right.save()

class Migration(migrations.Migration):

    dependencies = [
        ('base', '0002_auto_20151009_0326'),
        ('contenttypes', '0001_initial'),
        ('clicrdv', '0003_auto_20160920_0903'),
    ]

    operations = [
        migrations.CreateModel(
            name='NewClicRdv',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50)),
                ('slug', models.SlugField()),
                ('description', models.TextField()),
                ('log_level', models.CharField(default=b'NOTSET', max_length=10, verbose_name='Log Level', choices=[(b'NOTSET', b'NOTSET'), (b'DEBUG', b'DEBUG'), (b'INFO', b'INFO'), (b'WARNING', b'WARNING'), (b'ERROR', b'ERROR'), (b'CRITICAL', b'CRITICAL'), (b'FATAL', b'FATAL')])),
                ('server', models.CharField(default=b'sandbox.clicrdv.com', max_length=64, choices=[(b'www.clicrdv.com', b'Production (www.clicrdv.com)'), (b'sandbox.clicrdv.com', b'SandBox (sandbox.clicrdv.com)')])),
                ('group_id', models.IntegerField(default=0)),
                ('apikey', models.CharField(max_length=64)),
                ('username', models.CharField(max_length=64)),
                ('password', models.CharField(max_length=64)),
                ('websource', models.CharField(max_length=64, null=True, blank=True)),
                ('default_comment', models.CharField(max_length=250, null=True, blank=True)),
                ('users', models.ManyToManyField(to='base.ApiUser', blank=True)),
            ],
            options={
                'verbose_name': 'Clicrdv Agenda',
            },
        ),
        migrations.RunPython(migrate_clicrdv),
    ]
