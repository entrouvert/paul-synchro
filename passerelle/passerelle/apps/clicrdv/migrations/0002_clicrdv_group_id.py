# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('clicrdv', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='clicrdv',
            name='group_id',
            field=models.IntegerField(default=5242),  # aka vincennes migration
        ),
    ]
