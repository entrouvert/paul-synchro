# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('clicrdv', '0002_clicrdv_group_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clicrdv',
            name='group_id',
            field=models.IntegerField(default=0),
        ),
    ]
