# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('datasources', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClicRdv',
            fields=[
                ('basedatasource_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='datasources.BaseDataSource')),
                ('server', models.CharField(default=b'sandbox.clicrdv.com', max_length=64, choices=[(b'www.clicrdv.com', b'Production (www.clicrdv.com)'), (b'sandbox.clicrdv.com', b'SandBox (sandbox.clicrdv.com)')])),
                ('apikey', models.CharField(max_length=64)),
                ('username', models.CharField(max_length=64)),
                ('password', models.CharField(max_length=64)),
                ('websource', models.CharField(max_length=64, null=True, blank=True)),
                ('default_comment', models.CharField(max_length=250, null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Clicrdv Agenda',
            },
            bases=('datasources.basedatasource',),
        ),
    ]
