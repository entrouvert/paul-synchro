from django.views.decorators.csrf import csrf_exempt
from django.conf.urls import patterns, include, url

from passerelle.urls_utils import decorated_includes, required, app_enabled

from views import *

urlpatterns = patterns('',
    url(r'^(?P<slug>[\w,-]+)/$', ClicRdvDetailView.as_view(), name='clicrdv-view'),

    url(r'^(?P<slug>[\w,-]+)/interventions/(?P<intervention_id>\d+)/datetimes/$',
        DateTimesView.as_view(), name='clicrdv-datetimes'),
    url(r'^(?P<slug>[\w,-]+)/interventions/(?P<intervention_id>\d+)/dates/$',
        DatesView.as_view(), name='clicrdv-dates'),
    url(r'^(?P<slug>[\w,-]+)/interventions/(?P<intervention_id>\d+)/(?P<date>[\d-]+)/times$',
        TimesView.as_view(), name='clicrdv-times'),
    url(r'^(?P<slug>[\w,-]+)/interventions/(?P<intervention_id>\d+)/create$',
        csrf_exempt(CreateAppointmentView.as_view()), name='clicrdv-create-appointment'),
    url(r'^(?P<slug>[\w,-]+)/create$',
        csrf_exempt(CreateAppointmentView.as_view()), name='clicrdv-create-appointment-qs'),
    url(r'^(?P<slug>[\w,-]+)/(?P<appointment_id>\d+)/cancel$',
        CancelAppointmentView.as_view(), name='clicrdv-cancel-appointment'),
)
