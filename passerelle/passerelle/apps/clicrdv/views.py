import json

from django.views.generic.base import View
from django.views.generic.detail import SingleObjectMixin, DetailView

from passerelle import utils

from .models import ClicRdv


class ClicRdvDetailView(DetailView):
    model = ClicRdv


class DateTimesView(View, SingleObjectMixin):
    """list of free date&times in a intervention

    input: https//passerelle/clicrdv/foobar/interventions/887/datetimes
    """
    model = ClicRdv

    @utils.to_json('api')
    def get(self, request, intervention_id, *args, **kwargs):
        return self.get_object().get_datetimes(intervention_id)


class DatesView(View, SingleObjectMixin):
    """list of free dates in a intervention

    input: https//passerelle/clicrdv/foobar/interventions/887/dates
    output:
      { data: [ { id: '2014-05-07', text: "7 mai 2014" },
                { id: '2014-05-13', text: "13 mai 2014" } ], err: 0 }
    """
    model = ClicRdv

    @utils.to_json('api')
    def get(self, request, intervention_id, *args, **kwargs):
        return self.get_object().get_dates(intervention_id)


class TimesView(View, SingleObjectMixin):
    """list of free time in a date, for an intervention

    input: https//passerelle/clicrdv/foobar/interventions/887/2014-05-07/times
    output:
      { data: [ { id: '15:10:00', text: "15:10" },
               { id: '15:30:00', text: "15:30" } ], err: 0 }
    """
    model = ClicRdv

    @utils.to_json('api')
    def get(self, request, intervention_id, date, *args, **kwargs):
        return self.get_object().get_times(intervention_id, date)


class CreateAppointmentView(View, SingleObjectMixin):
    """create an appointment

    input (POST: URL and payload)
      https//passerelle/data/clicrdv-title/interventions/887/create[?websource=Web]
      https//passerelle/data/clicrdv-title/create?[intervention=887][&websource=Web]
      {
        'clicrdv_intervention_raw': ..., # optional if present in URL
        'clicrdv_firstname': ...,
        'clicrdv_lastname': ...,
        'clicrdv_email': ...,
        'clicrdv_firstphone': ...,
        'clicrdv_secondphone': ...,
        'clicrdv_date_raw': ..., 'clicrdv_time_raw': ..., # or 'clicrdv_datetime_raw': ...,
        'clicrdv_comments': ..., # optional
        'clicrdc_fiche_xxx': ... # add a optional "xxx" value in fiche
      }

      The various keys will also be looked in the 'fields' and 'extra'
      dictionaries, if existing.  Order is root/extra/fields.

    output:
      { data: { 'success': true, 'appointment_id': 123 }, err: 0 }
    """
    model = ClicRdv

    @utils.protected_api('can_manage_appointment')
    @utils.to_json('api')
    def post(self, request, intervention_id=None, *args, **kwargs):
        if intervention_id is None:
            intervention_id = self.request.GET.get('intervention')
        data = json.loads(request.body)
        return self.get_object().create_appointment(
                intervention_id,
                self.request.GET.get('websource'),
                data)


class CancelAppointmentView(View, SingleObjectMixin):
    """cancel an appointment

    input: https//passerelle/clicrdv/foobar/1234/cancel
    output:
      { data: { 'success': true }, err: 0 }
    """
    model = ClicRdv

    @utils.protected_api('can_manage_appointment')
    @utils.to_json('api')
    def get(self, request, appointment_id, *args, **kwargs):
        return self.get_object().cancel(appointment_id)
