from django.conf.urls import patterns, url
from views import *

urlpatterns = patterns('',
    url(r'^(?P<slug>[\w,-]+)/$', ChoositDetailView.as_view(), name='choosit-view'),
    url(r'^register/(?P<slug>[\w,-]+)/$', ChoositRegisterDetailView.as_view(), name='choosit-register-view'),
    url(r'^register/(?P<slug>[\w,-]+)/register$', ChoositRegisterView.as_view(), name='choosit-register'),
)

management_urlpatterns = patterns('',
    url(r'^register/add$', ChoositRegisterCreateView.as_view(), name='choosit-register-add'),
    url(r'^register/(?P<slug>[\w,-]+)/edit$', ChoositRegisterUpdateView.as_view(), name='choosit-register-edit'),
    url(r'^register/(?P<slug>[\w,-]+)/delete$', ChoositRegisterDeleteView.as_view(), name='choosit-register-delete'),
)
