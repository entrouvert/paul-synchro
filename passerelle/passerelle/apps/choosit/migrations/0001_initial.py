# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import passerelle.sms


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ChoositRegisterGateway',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50)),
                ('slug', models.SlugField()),
                ('description', models.TextField()),
                ('url', models.CharField(max_length=200)),
                ('key', models.CharField(max_length=64)),
            ],
            options={
                'db_table': 'registration_choosit',
                'verbose_name': 'Choosit Registration',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ChoositRegisterNewsletter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=16)),
                ('description', models.CharField(max_length=128, blank=True)),
            ],
            options={
                'db_table': 'newsletter_choosit',
                'verbose_name': 'Choosit Registration',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ChoositSMSGateway',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50)),
                ('slug', models.SlugField()),
                ('description', models.TextField()),
                ('key', models.CharField(max_length=64)),
                ('default_country_code', models.CharField(default='33', max_length=3)),
                ('users', models.ManyToManyField(to='base.ApiUser', blank=True)),
            ],
            options={
                'db_table': 'sms_choosit',
                'verbose_name': 'Choosit',
            },
            bases=(models.Model, passerelle.sms.SMSGatewayMixin),
        ),
        migrations.AddField(
            model_name='choositregistergateway',
            name='newsletters',
            field=models.ManyToManyField(to='choosit.ChoositRegisterNewsletter', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='choositregistergateway',
            name='users',
            field=models.ManyToManyField(to='base.ApiUser', blank=True),
            preserve_default=True,
        ),
    ]
