# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('choosit', '0002_auto_20160316_0528'),
    ]

    operations = [
        migrations.AlterField(
            model_name='choositregistergateway',
            name='log_level',
            field=models.CharField(default=b'NOTSET', max_length=10, verbose_name='Log Level', choices=[(b'DEBUG', b'DEBUG'), (b'INFO', b'INFO')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='choositsmsgateway',
            name='log_level',
            field=models.CharField(default=b'NOTSET', max_length=10, verbose_name='Log Level', choices=[(b'DEBUG', b'DEBUG'), (b'INFO', b'INFO')]),
            preserve_default=True,
        ),
    ]
