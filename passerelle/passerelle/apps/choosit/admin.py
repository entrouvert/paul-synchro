from django.contrib import admin

from .models import ChoositSMSGateway, ChoositRegisterGateway, ChoositRegisterNewsletter

class ChoositSMSGatewayAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    list_display = ['title', 'slug', 'description', 'key',
            'default_country_code']

admin.site.register(ChoositSMSGateway, ChoositSMSGatewayAdmin)



class ChoositRegisterGatewayAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(ChoositRegisterGateway, ChoositRegisterGatewayAdmin)
admin.site.register(ChoositRegisterNewsletter)

