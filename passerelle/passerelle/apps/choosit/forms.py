from django.utils.text import slugify
from django import forms

from .models import ChoositRegisterGateway

class ChoositRegisterGatewayForm(forms.ModelForm):
    class Meta:
        model = ChoositRegisterGateway
        exclude = ('slug', 'users')

    def save(self, commit=True):
        if not self.instance.slug:
            self.instance.slug = slugify(self.instance.title)
        return super(ChoositRegisterGatewayForm, self).save(commit=commit)
