# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Pastell',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50)),
                ('slug', models.SlugField()),
                ('description', models.TextField()),
                ('url', models.CharField(help_text='Pastell URL', max_length=128, verbose_name='URL')),
                ('verify_cert', models.BooleanField(default=True, verbose_name='Check HTTPS Certificate validity')),
                ('username', models.CharField(max_length=128, verbose_name='Username')),
                ('password', models.CharField(max_length=128, verbose_name='Password')),
                ('keystore', models.FileField(help_text='Certificate and private key in PEM format', upload_to=b'pastell', null=True, verbose_name='Keystore')),
                ('document_type', models.CharField(max_length=128, verbose_name='Document Type', blank=True)),
                ('document_fields', jsonfield.fields.JSONField(null=True, verbose_name='Fields')),
                ('users', models.ManyToManyField(to='base.ApiUser', blank=True)),
            ],
            options={
                'verbose_name': 'Pastell',
            },
            bases=(models.Model,),
        ),
    ]
