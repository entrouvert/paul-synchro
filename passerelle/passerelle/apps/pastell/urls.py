from django.views.decorators.csrf import csrf_exempt
from django.conf.urls import patterns, url
from views import *

urlpatterns = patterns('',
    url(r'^(?P<slug>[\w,-]+)/$', PastellDetailView.as_view(), name='pastell-view'),

    url(r'^(?P<slug>[\w,-]+)/post/$', csrf_exempt(PostView.as_view()), name='pastell-post'),
)

management_urlpatterns = patterns('',
    url(r'^(?P<slug>[\w,-]+)/type$', PastellTypeView.as_view(), name='pastell-type'),
    url(r'^(?P<slug>[\w,-]+)/fields$', PastellFieldsView.as_view(), name='pastell-fields'),
)
