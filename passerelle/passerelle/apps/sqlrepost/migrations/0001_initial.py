# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('repost', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SQLBinding',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=32)),
                ('expression', models.TextField()),
            ],
            options={
                'ordering': ('id',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SQLRepost',
            fields=[
                ('baserepost_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='repost.BaseRepost')),
                ('engine_options', models.CharField(help_text="a pythonic({'key': 'value'} form) dictionnary that will be passed to the database driver", max_length=64)),
            ],
            options={
                'abstract': False,
            },
            bases=('repost.baserepost',),
        ),
        migrations.CreateModel(
            name='SQLStatement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('statement', models.TextField()),
                ('output_var', models.CharField(max_length=64, null=True, blank=True)),
                ('mapping', models.IntegerField(choices=[(0, b'first row to dict'), (1, b'all rows to dict'), (2, b'table with header as first row')])),
                ('sqlrepost', models.ForeignKey(related_name='statements', to='sqlrepost.SQLRepost')),
            ],
            options={
                'ordering': ('id',),
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='sqlbinding',
            name='sqlreport',
            field=models.ForeignKey(related_name='bindings', to='sqlrepost.SQLRepost'),
            preserve_default=True,
        ),
    ]
