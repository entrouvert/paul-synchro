# -*- encoding: utf-8 -*-

"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

#from django.test import TestCase
from django_webtest import WebTest

from django.core.urlresolvers import reverse

from subprocess import call
import os

class fetchTest(WebTest):

    fixtures = ['reposts', 'sqlreposts']

    def testFetchUserChildren(self):
        user_id = 137
        response = self.app.post_json(reverse('sqlrepost',
                                              kwargs = {'slug': 'children'}),
                                      dict(user = user_id)
                           )
        self.assertEqual(response.json.get(u'err'), 0)

    def testFetchUserChildrenMoreAttributes(self):
        """
        Same as testFetchUserChildren but adds more attributes in json
        """
        user_id = 137
        response = self.app.post_json(reverse('sqlrepost',
                                              kwargs = {'slug': 'children'}),
                                      dict(user = user_id,
                                           more = 'data',
                                           one_more = 'more_data')
                           )
        self.assertEqual(response.json.get(u'err'), 0)

    def testFetchUserInvoices(self):
        data = dict(user = 137)
        response = self.app.post_json(reverse('sqlrepost',
                                              kwargs = {'slug': 'invoices'}),
                                      data)
        self.assertEqual(response.json.get(u'err'), 0)

    def testFetchUserInfos(self):
        data = dict(id = 137)
        response = self.app.post_json(reverse('sqlrepost',
                                              kwargs = {'slug': 'user'}), data)
        self.assertEqual(response.json.get(u'err'), 0)

    def testFetchPersonsbyPostCode(self):
        data = dict(code_postal = 45100, commune = u'ORLEANS')
        response = self.app.post_json(reverse('sqlrepost',
                                              kwargs = {'slug': 'put'}), data)
        self.assertEqual(response.json.get(u'err'), 0)

    def tearDown(self):
        pass
