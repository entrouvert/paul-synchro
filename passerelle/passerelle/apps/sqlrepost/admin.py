from django.contrib import admin

from models import SQLRepost, SQLStatement, SQLBinding

class SQLStatementInline(admin.TabularInline):
    model = SQLStatement


class SQLBindingInline(admin.TabularInline):
    model = SQLBinding

class SQLRepostAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title', )}

    inlines = (SQLStatementInline, SQLBindingInline)
    extra = 3

admin.site.register(SQLRepost, SQLRepostAdmin)
