# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import passerelle.sms


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='OVHSMSGateway',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50)),
                ('slug', models.SlugField()),
                ('description', models.TextField()),
                ('account', models.CharField(max_length=64)),
                ('username', models.CharField(max_length=64)),
                ('password', models.CharField(max_length=64)),
                ('msg_class', models.IntegerField(default=1, verbose_name='message class', choices=[(0, 'Message are directly shown to users on phone screen at reception. The message is never stored, neither in the phone memory nor in the SIM card. It is deleted as soon as the user validate the display.'), (1, 'Messages are stored in the phone memory, or in the SIM card if the memory is full. '), (2, 'Messages are stored in the SIM card.'), (3, 'Messages are stored in external storage like a PDA or a PC.')])),
                ('credit_threshold_alert', models.PositiveIntegerField(default=100)),
                ('default_country_code', models.CharField(default='33', max_length=3)),
                ('credit_left', models.PositiveIntegerField(default=0)),
                ('users', models.ManyToManyField(to='base.ApiUser', blank=True)),
            ],
            options={
                'db_table': 'sms_ovh',
                'verbose_name': 'OVH',
            },
            bases=(models.Model, passerelle.sms.SMSGatewayMixin),
        ),
    ]
