# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('repost', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='MakoPrototypeRepost',
            fields=[
                ('baserepost_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='repost.BaseRepost')),
                ('input_template', models.TextField(blank=True)),
                ('output_template', models.TextField(blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('repost.baserepost',),
        ),
        migrations.CreateModel(
            name='MakoRepost',
            fields=[
                ('baserepost_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='repost.BaseRepost')),
            ],
            options={
                'abstract': False,
            },
            bases=('repost.baserepost',),
        ),
    ]
