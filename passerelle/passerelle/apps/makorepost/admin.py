from django.contrib import admin

from models import MakoPrototypeRepost, MakoRepost

class MakoPrototypeRepostAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(MakoPrototypeRepost, MakoPrototypeRepostAdmin)

class MakoRepostAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(MakoRepost, MakoRepostAdmin)

