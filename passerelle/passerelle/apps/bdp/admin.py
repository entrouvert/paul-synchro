from django.contrib import admin

from models import Bdp


class BdpAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(Bdp, BdpAdmin)
