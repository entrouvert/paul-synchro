# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ldap', '0005_auto_20170301_0309'),
    ]

    operations = [
        migrations.AddField(
            model_name='query',
            name='ldif_file',
            field=models.FileField(upload_to=b'ldif', verbose_name=b'LDIF File', blank=True),
        ),
        migrations.AddField(
            model_name='query',
            name='operation',
            field=models.CharField(default=b'1', max_length=1, verbose_name=b'Operation type', choices=[(b'1', b'Search'), (b'2', b'Add'), (b'3', b'Modify'), (b'4', b'Delete')]),
        ),
        migrations.AlterField(
            model_name='query',
            name='attributes',
            field=models.TextField(default=b'ALL_ATTRIBUTES', help_text=b'List of attributes to retrieve for matching entries.', verbose_name=b'Attributes'),
        ),
        migrations.AlterField(
            model_name='query',
            name='distinguished_name',
            field=models.TextField(default=b'ou=People,dc=entrouvert,dc=lan', help_text=b'Distinguished name for the LDAP operation', verbose_name=b'Distinguished name', blank=True),
        ),
        migrations.AlterField(
            model_name='query',
            name='scope',
            field=models.CharField(default=b'1', max_length=1, verbose_name=b'Scope', choices=[(b'1', b'BASE'), (b'2', b'LEVEL'), (b'3', b'SUBTREE')]),
        ),
    ]
