# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('ldap', '0002_auto_20170224_0505'),
    ]

    operations = [
        migrations.CreateModel(
            name='Query',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', models.SlugField(verbose_name=b'Name (slug)')),
                ('label', models.CharField(max_length=100, verbose_name=b'Label')),
                ('description', models.TextField(verbose_name=b'Description', blank=True)),
                ('server', models.CharField(max_length=200, verbose_name=b'Server')),
                ('user', models.CharField(max_length=200, verbose_name=b'User')),
                ('password', models.CharField(max_length=50, verbose_name=b'Password')),
                ('distinguished_name', models.TextField(help_text=b'Distinguished name for the LDAP operation', verbose_name=b'Distinguished name', blank=True)),
                ('filters', models.TextField(help_text=b'List of filter clauses (LDAP RFC4515 expression)', verbose_name=b'Filters', blank=True)),
                ('attributes', models.TextField(help_text=b'List of attributes to retrieve for matching entries.', verbose_name=b'Attributes', blank=True)),
                ('scope', models.CharField(max_length=1, choices=[(b'1', b'BASE'), (b'2', b'LEVEL'), (b'3', b'SUBTREE')])),
            ],
            options={
                'ordering': ['slug'],
            },
        ),
        migrations.AddField(
            model_name='ldapresource',
            name='_dialect_options',
            field=jsonfield.fields.JSONField(null=True, editable=False),
        ),
        migrations.AddField(
            model_name='ldapresource',
            name='ldif_file',
            field=models.FileField(upload_to=b'ldif', verbose_name=b'LDIF File', blank=True),
        ),
        migrations.AddField(
            model_name='query',
            name='resource',
            field=models.ForeignKey(to='ldap.LDAPResource'),
        ),
    ]
