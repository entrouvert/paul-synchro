# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ldap', '0004_auto_20170228_1050'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ldapresource',
            name='ldif_file',
        ),
        migrations.RemoveField(
            model_name='query',
            name='password',
        ),
        migrations.RemoveField(
            model_name='query',
            name='server',
        ),
        migrations.RemoveField(
            model_name='query',
            name='user',
        ),
    ]
