# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ldap', '0003_auto_20170228_1019'),
    ]

    operations = [
        migrations.AddField(
            model_name='ldapresource',
            name='password',
            field=models.CharField(max_length=50, verbose_name=b'Password', blank=True),
        ),
        migrations.AddField(
            model_name='ldapresource',
            name='server',
            field=models.CharField(default=b'localhost', max_length=200, verbose_name=b'Server name'),
        ),
        migrations.AddField(
            model_name='ldapresource',
            name='user',
            field=models.CharField(max_length=200, verbose_name=b'Username (full DN)', blank=True),
        ),
    ]
