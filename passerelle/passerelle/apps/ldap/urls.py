# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2016 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from views import *

from django.conf.urls import patterns, include, url

from .views import *

urlpatterns = patterns('',
    url(r'^(?P<slug>[\w,-]+)/data$', LDAPView.as_view(), name='ldap-data'),
    url(r'^(?P<slug>[\w,-]+)/wcs/(?P<wcs_entry_id>[\d]+)/$',
        wcs, name='wcs'),
    url(r'^(?P<slug>[\w,-]+)/wcs/$',
        wcs, name='wcs'),
)

management_urlpatterns = patterns('',
    url(r'^(?P<connector_slug>[\w,-]+)/download/$',
        LDAPDownload.as_view(), name='ldap-download'),
    url(r'^(?P<connector_slug>[\w,-]+)/queries/new/$',
        NewQueryView.as_view(), name='ldap-new-query'),
    url(r'^(?P<connector_slug>[\w,-]+)/queries/(?P<pk>[\w,-]+)/$',
        UpdateQueryView.as_view(), name='ldap-edit-query'),
    #url(r'^view-last$', LDAPView.as_view(), name='view-last'),
)
