# Passerelle - uniform access to data and services
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import csv
import cStringIO
import datetime
from decimal import Decimal
import json
import os
import sys
import zipfile

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from ..models import Invoice

def u(s):
    return unicode(s, 'iso-8859-15')

class Loader(object):
    def __init__(self, connector):
        self.connector = connector

    def clean(self, archive):
        if not 'data_full.csv' in archive.namelist():
            raise ValidationError(_('Missing data_full.csv file in zip.'))

    def load(self, archive):
        archive_files = archive.namelist()

        csvfile = cStringIO.StringIO(archive.read('data_full.csv'))
        csvreader = csv.reader(csvfile, delimiter='\t')
        first_row = csvreader.next()
        csvfile.seek(0)
        csvreader = csv.DictReader(csvfile, delimiter='\t', fieldnames=first_row)
        csvreader.next()
        for row in csvreader:
            invoice = {}
            invoice['total_amount'] = row['MNT_FACTURE_FAC'].replace(',', '.')
            invoice['amount'] = str(Decimal(invoice['total_amount']) -
                                    Decimal(row['MNT_REGLE_FAC'].replace(',', '.')))
            invoice['paid'] = bool(Decimal(invoice['amount']) == 0)
            invoice['issue_date'] = datetime.datetime.strptime(
                    row['DAT_GENERATION_FAC'], '%d/%m/%Y').strftime('%Y-%m-%d')
            invoice['pay_limit_date'] = datetime.datetime.strptime(
                    row['DAT_LIMITEPAIE_FAC'], '%d/%m/%Y').strftime('%Y-%m-%d')
            invoice['online_payment'] = True
            invoice['no_online_payment_reason'] = None
            if not invoice['paid']:
                if row['MNT_REGLE_FAC'] != '0,00':
                    # it has been partially paid, we don't allow the rest to be
                    # paid.
                    invoice['online_payment'] = False
                elif row['DAT_PRELEVEMENT_FAC'] or row['LIB_PRELEVAUTO_LST'] == 'Oui':
                    invoice['online_payment'] = False
                    invoice['no_online_payment_reason'] = 'autobilling'

            obj, created = Invoice.objects.update_or_create(resource=self.connector,
                    external_id=row['ID_FAC'], defaults=invoice)
            invoice_filename = '%s_%s.pdf' % (
                    datetime.datetime.strptime(row['DAT_DEBUT_PGE'], '%d/%m/%Y').strftime('%Y-%m'),
                    row['ID_FAC'])
            if invoice_filename in archive_files:
                obj.write_pdf(archive.read(invoice_filename))
