# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('family', '0006_invoice_no_online_payment_reason'),
    ]

    operations = [
        migrations.AddField(
            model_name='adult',
            name='creation_timestamp',
            field=models.DateTimeField(default=datetime.datetime(2016, 11, 22, 17, 15, 39, 968134, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='adult',
            name='update_timestamp',
            field=models.DateTimeField(default=datetime.datetime(2016, 11, 22, 17, 15, 43, 378414, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='child',
            name='creation_timestamp',
            field=models.DateTimeField(default=datetime.datetime(2016, 11, 22, 17, 15, 47, 471108, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='child',
            name='update_timestamp',
            field=models.DateTimeField(default=datetime.datetime(2016, 11, 22, 17, 15, 52, 71574, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='family',
            name='creation_timestamp',
            field=models.DateTimeField(default=datetime.datetime(2016, 11, 22, 17, 15, 55, 626407, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='family',
            name='update_timestamp',
            field=models.DateTimeField(default=datetime.datetime(2016, 11, 22, 17, 16, 0, 117260, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='invoice',
            name='creation_timestamp',
            field=models.DateTimeField(default=datetime.datetime(2016, 11, 22, 17, 16, 3, 925696, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='invoice',
            name='update_timestamp',
            field=models.DateTimeField(default=datetime.datetime(2016, 11, 22, 17, 16, 7, 805483, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
    ]
