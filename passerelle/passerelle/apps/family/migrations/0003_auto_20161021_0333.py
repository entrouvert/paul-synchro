# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('family', '0002_auto_20161020_0426'),
    ]

    operations = [
        migrations.AddField(
            model_name='genericfamily',
            name='file_format',
            field=models.CharField(default=b'native', max_length=40, verbose_name='File Format', choices=[(b'native', 'Native'), (b'concerto_fondettes', 'Concerto extract from Fondettes'), (b'concerto_orleans', 'Concerto extract from Orl\xe9ans')]),
        ),
        migrations.AlterField(
            model_name='invoice',
            name='family',
            field=models.ForeignKey(to='family.Family', null=True),
        ),
    ]
