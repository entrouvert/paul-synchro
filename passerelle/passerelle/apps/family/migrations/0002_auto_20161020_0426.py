# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('family', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='invoice',
            old_name='expiration_date',
            new_name='pay_limit_date',
        ),
        migrations.AlterField(
            model_name='invoice',
            name='pay_limit_date',
            field=models.DateField(null=True, verbose_name='Due date'),
        ),
    ]
