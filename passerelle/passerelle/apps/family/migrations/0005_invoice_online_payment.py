# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('family', '0004_invoice_paid'),
    ]

    operations = [
        migrations.AddField(
            model_name='invoice',
            name='online_payment',
            field=models.BooleanField(default=True, verbose_name='Online payment'),
        ),
    ]
