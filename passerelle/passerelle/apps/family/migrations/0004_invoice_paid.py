# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('family', '0003_auto_20161021_0333'),
    ]

    operations = [
        migrations.AddField(
            model_name='invoice',
            name='paid',
            field=models.BooleanField(default=False, verbose_name='Paid'),
        ),
    ]
