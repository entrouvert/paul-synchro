# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0002_auto_20151009_0326'),
    ]

    operations = [
        migrations.CreateModel(
            name='Adult',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('external_id', models.CharField(max_length=32, verbose_name="Person's external id", db_index=True)),
                ('first_name', models.CharField(max_length=64, verbose_name='First name')),
                ('last_name', models.CharField(max_length=64, verbose_name='Last name')),
                ('sex', models.CharField(max_length=1, verbose_name='Sex', choices=[(b'M', 'Male'), (b'F', 'Female')])),
                ('birthdate', models.DateField(null=True, verbose_name='Birthdate', blank=True)),
                ('phone', models.CharField(max_length=32, null=True, verbose_name='Phone')),
                ('cellphone', models.CharField(max_length=32, null=True, verbose_name='Cellphone')),
                ('street_number', models.CharField(max_length=32, null=True, verbose_name='Street number')),
                ('street_name', models.CharField(max_length=128, null=True, verbose_name='Street name')),
                ('address_complement', models.CharField(max_length=64, null=True, verbose_name='Address complement')),
                ('zipcode', models.CharField(max_length=16, null=True, verbose_name='Zipcode')),
                ('city', models.CharField(max_length=64, null=True, verbose_name='City')),
                ('country', models.CharField(max_length=128, null=True, verbose_name='Country')),
                ('email', models.EmailField(max_length=254, null=True, verbose_name='Email')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Child',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('external_id', models.CharField(max_length=32, verbose_name="Person's external id", db_index=True)),
                ('first_name', models.CharField(max_length=64, verbose_name='First name')),
                ('last_name', models.CharField(max_length=64, verbose_name='Last name')),
                ('sex', models.CharField(max_length=1, verbose_name='Sex', choices=[(b'M', 'Male'), (b'F', 'Female')])),
                ('birthdate', models.DateField(null=True, verbose_name='Birthdate', blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Family',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('external_id', models.CharField(max_length=16, verbose_name='External identifier', db_index=True)),
                ('login', models.CharField(max_length=64, null=True, verbose_name='Login')),
                ('password', models.CharField(max_length=64, null=True, verbose_name='Password')),
                ('street_number', models.CharField(max_length=32, null=True, verbose_name='Street number')),
                ('street_name', models.CharField(max_length=128, null=True, verbose_name='Street name')),
                ('address_complement', models.CharField(max_length=64, null=True, verbose_name='Address complement')),
                ('zipcode', models.CharField(max_length=16, null=True, verbose_name='Zipcode')),
                ('city', models.CharField(max_length=64, null=True, verbose_name='City')),
                ('family_quotient', models.DecimalField(default=0, verbose_name='Family quotient', max_digits=10, decimal_places=2)),
            ],
        ),
        migrations.CreateModel(
            name='FamilyLink',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name_id', models.CharField(max_length=256)),
                ('family', models.ForeignKey(to='family.Family')),
            ],
        ),
        migrations.CreateModel(
            name='GenericFamily',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50)),
                ('slug', models.SlugField()),
                ('description', models.TextField()),
                ('log_level', models.CharField(default=b'NOTSET', max_length=10, verbose_name='Log Level', choices=[(b'NOTSET', b'NOTSET'), (b'DEBUG', b'DEBUG'), (b'INFO', b'INFO'), (b'WARNING', b'WARNING'), (b'ERROR', b'ERROR'), (b'CRITICAL', b'CRITICAL'), (b'FATAL', b'FATAL')])),
                ('archive', models.FileField(upload_to=b'archives', verbose_name='Data Archive')),
                ('users', models.ManyToManyField(to='base.ApiUser', blank=True)),
            ],
            options={
                'verbose_name': 'Generic Family Connector',
            },
        ),
        migrations.CreateModel(
            name='Invoice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('external_id', models.CharField(max_length=128, verbose_name='External id', db_index=True)),
                ('label', models.CharField(max_length=128, null=True, verbose_name='Label')),
                ('issue_date', models.DateField(null=True, verbose_name='Issue date')),
                ('expiration_date', models.DateField(null=True, verbose_name='Expiration date')),
                ('litigation_date', models.DateField(null=True, verbose_name='Litigation date')),
                ('total_amount', models.DecimalField(default=0, verbose_name='Total amount', max_digits=6, decimal_places=2)),
                ('amount', models.DecimalField(default=0, verbose_name='Amount', max_digits=6, decimal_places=2)),
                ('payment_date', models.DateTimeField(null=True, verbose_name='Payment date')),
                ('autobilling', models.BooleanField(default=False, verbose_name='Autobilling')),
                ('payment_transaction_id', models.CharField(max_length=128, null=True, verbose_name='Payment transaction id')),
                ('family', models.ForeignKey(to='family.Family')),
                ('resource', models.ForeignKey(to='family.GenericFamily')),
            ],
            options={
                'ordering': ['issue_date'],
            },
        ),
        migrations.AddField(
            model_name='familylink',
            name='resource',
            field=models.ForeignKey(to='family.GenericFamily'),
        ),
        migrations.AddField(
            model_name='family',
            name='resource',
            field=models.ForeignKey(to='family.GenericFamily'),
        ),
        migrations.AddField(
            model_name='child',
            name='family',
            field=models.ForeignKey(to='family.Family'),
        ),
        migrations.AddField(
            model_name='adult',
            name='family',
            field=models.ForeignKey(to='family.Family'),
        ),
    ]
