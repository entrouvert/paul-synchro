# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('family', '0005_invoice_online_payment'),
    ]

    operations = [
        migrations.AddField(
            model_name='invoice',
            name='no_online_payment_reason',
            field=models.CharField(max_length=100, null=True, verbose_name='No online payment reason'),
        ),
    ]
