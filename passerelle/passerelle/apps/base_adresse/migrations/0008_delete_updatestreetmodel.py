# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base_adresse', '0007_auto_20160729_1540'),
    ]

    operations = [
        migrations.DeleteModel(
            name='UpdateStreetModel',
        ),
    ]
