# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base_adresse', '0002_auto_20150705_0330'),
    ]

    operations = [
        migrations.AddField(
            model_name='baseaddresse',
            name='log_level',
            field=models.CharField(default=b'NOTSET', max_length=10, verbose_name='Debug Enabled', blank=True, choices=[(b'DEBUG', b'DEBUG'), (b'INFO', b'INFO')]),
            preserve_default=True,
        ),
    ]
