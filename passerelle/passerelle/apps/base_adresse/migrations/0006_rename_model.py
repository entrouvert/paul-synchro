# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base_adresse', '0005_auto_20160407_0456'),
    ]

    operations = [
        migrations.RenameModel('BaseAddresse', 'BaseAdresse')
    ]
