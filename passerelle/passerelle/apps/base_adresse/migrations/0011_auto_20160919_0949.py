# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base_adresse', '0010_auto_20160914_0826'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='streetmodel',
            options={'ordering': ['unaccent_name', 'name']},
        ),
    ]
