# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base_adresse', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='baseaddresse',
            name='service_url',
            field=models.CharField(default=b'https://api-adresse.data.gouv.fr/', help_text='Base Adresse Web Service URL', max_length=128, verbose_name='Service URL'),
            preserve_default=True,
        ),
    ]
