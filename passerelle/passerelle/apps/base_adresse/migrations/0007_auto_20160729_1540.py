# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base_adresse', '0006_rename_model'),
    ]

    operations = [
        migrations.CreateModel(
            name='StreetModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('city', models.CharField(max_length=100, verbose_name='City')),
                ('name', models.CharField(max_length=150, verbose_name='Street name')),
                ('zipcode', models.CharField(max_length=5, verbose_name='Postal code')),
                ('type', models.CharField(max_length=30, verbose_name='Street type')),
                ('citycode', models.CharField(max_length=5, verbose_name='City Code')),
                ('last_update', models.DateTimeField(null=True, verbose_name='Last update')),
            ],
        ),
        migrations.CreateModel(
            name='UpdateStreetModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('zipcode', models.CharField(max_length=5, verbose_name='Postal code')),
                ('start_time', models.DateTimeField(null=True, verbose_name='Start of update')),
                ('end_time', models.DateTimeField(null=True, verbose_name='End of update')),
            ],
        ),
        migrations.AddField(
            model_name='baseadresse',
            name='zipcode',
            field=models.CharField(help_text='Postal code to get the streets from.', max_length=5, verbose_name='Postal code to get streets', blank=True),
        ),
    ]
