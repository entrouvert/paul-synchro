from django.contrib import admin

from models import Gdc


class GdcAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(Gdc, GdcAdmin)
