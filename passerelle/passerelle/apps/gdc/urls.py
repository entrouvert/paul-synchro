from django.views.decorators.csrf import csrf_exempt
from django.conf.urls import patterns, url
from views import *

urlpatterns = patterns('',
    url(r'^(?P<slug>[\w,-]+)/$', GdcDetailView.as_view(), name='gdc-view'),

    url(r'^(?P<slug>[\w,-]+)/voies/(?P<insee>\d+)$', VoiesView.as_view(), name='gdc-voies'),
    url(r'^(?P<slug>[\w,-]+)/post/demande$', csrf_exempt(PostDemandeView.as_view()), name='gdc-post'),
    url(r'^(?P<slug>[\w,-]+)/status/(?P<ref>\d+)', StatusView.as_view(), name='gdc-status'),
)
