from django.contrib import admin

from .models import OxydSMSGateway

class OxydSMSGatewayAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    list_display = ['title', 'slug', 'description', 'username', 'password',
            'default_country_code']

admin.site.register(OxydSMSGateway, OxydSMSGatewayAdmin)
