# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import passerelle.sms


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='OxydSMSGateway',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50)),
                ('slug', models.SlugField()),
                ('description', models.TextField()),
                ('username', models.CharField(max_length=64)),
                ('password', models.CharField(max_length=64)),
                ('default_country_code', models.CharField(default='33', max_length=3)),
                ('users', models.ManyToManyField(to='base.ApiUser', blank=True)),
            ],
            options={
                'db_table': 'sms_oxyd',
                'verbose_name': 'Oxyd',
            },
            bases=(models.Model, passerelle.sms.SMSGatewayMixin),
        ),
    ]
