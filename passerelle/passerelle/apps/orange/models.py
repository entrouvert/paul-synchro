from StringIO import StringIO

from django.core.files import File
from django.utils.translation import ugettext_lazy as _
from django.db import models

from passerelle.base.models import BaseResource
from passerelle.sms import SMSGatewayMixin

from . import soap


class OrangeSMSGateway(BaseResource, SMSGatewayMixin):
    keystore = models.FileField(upload_to='orange', blank=True, null=True,
                                verbose_name=_('Keystore'),
                                help_text=_('Certificate and private key in PEM format'))
    default_country_code = '33'

    URL = ('https://www.api-contact-everyone.fr.orange-business.com/ContactEveryone/services'
           '/MultiDiffusionWS')

    manager_view_template_name = 'passerelle/manage/messages_service_view.html'

    class Meta:
        verbose_name = 'Orange'
        db_table = 'sms_orange'

    @classmethod
    def get_icon_class(cls):
        return 'phone'

    def send_msg(self, text, sender, destinations):
        """Send a SMS using the Orange provider"""
        # unfortunately it lacks a batch API...
        destinations = self.clean_numbers(destinations, self.default_country_code)
        return soap.ContactEveryoneSoap(instance=self).send_advanced_message(destinations, sender,
                                                                             text)
