# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orange', '0002_orangesmsgateway_log_level'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orangesmsgateway',
            name='log_level',
            field=models.CharField(default=b'NOTSET', max_length=10, verbose_name='Log Level', choices=[(b'DEBUG', b'DEBUG'), (b'INFO', b'INFO')]),
            preserve_default=True,
        ),
    ]
