# Passerelle - uniform access to data and services
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import patterns, include, url

from .views import *

urlpatterns = patterns('',
    url(r'^(?P<slug>[\w,-]+)/$', SolisAPADetailView.as_view(),
        name='solis-apa-view'),
    url(r'^(?P<slug>[\w,-]+)/communes/$', CommunesView.as_view(),
        name='solis-apa-communes'),
    url(r'^(?P<slug>[\w,-]+)/lieux/$', LieuxView.as_view(),
        name='solis-apa-lieux'),
    url(r'^(?P<slug>[\w,-]+)/homonymes/$', HomonymesView.as_view(),
        name='solis-apa-homonymes'),
    url(r'^(?P<slug>[\w,-]+)/referential/(?P<reference_name>[\w,-]+)/$',
        ReferentialView.as_view(), name='solis-apa-referential'),
    url(r'^(?P<slug>[\w,-]+)/suivi/(?P<suivi_type>[\w,-]+)/$',
        SuiviView.as_view(), name='solis-apa-suivi'),
    url(r'^(?P<slug>[\w,-]+)/integration/$', IntegrationView.as_view(),
        name='solis-apa-integration')
)
