# Passerelle - uniform access to data and services
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.views.generic import DetailView as GenericDetailView, View
from django.views.generic.detail import SingleObjectMixin
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.utils.translation import ugettext_lazy as _

from passerelle import utils

from .models import SolisAPA


class CommissionTypeNotFound(Exception):
    http_status = 404


class SolisAPADetailView(GenericDetailView):
    model = SolisAPA
    template_name = 'passerelle/contrib/solis_apa/detail.html'


class DetailView(GenericDetailView):
    model = SolisAPA

    def get_data(self, request, *args, **kwargs):
        raise NotImplementedError

    @utils.protected_api('can_access')
    @utils.to_json('api')
    def get(self, request, *args, **kwargs):
        return self.get_data(request, *args, **kwargs)

    def _get_params(self, request, *params):
        return [ request.GET.get(item, None) for item in params]


class CommunesView(DetailView):
    def get_data(self, request, *args, **kwargs):
        params = self._get_params(request, 'q', 'code_dep')
        return self.get_object().get_communes(*params)


class LieuxView(DetailView):
    def get_data(self, request, *args, **kwargs):
        params = self._get_params(request, 'q', 'commune', 'departement')
        return self.get_object().get_lieux(*params)


class HomonymesView(DetailView):
    def get_data(self, request, *args, **kwargs):
        params = self._get_params(request, 'nom', 'prenom', 'dn')
        return self.get_object().get_homonymes(*params)


class CaisseDeRetraiteView(DetailView):
    def get_data(self, request, *args, **kwargs):
        return self.get_object().get_caisse_retraite()


class OrganismeDeTutelleView(DetailView):
    def get_data(self, request, *args, **kwargs):
        return self.get_object().get_organisme_tutelle()


class EtablissementView(DetailView):
    def get_data(self, request, *args, **kwargs):
        return self.get_object().get_etablissement()


class ReferentialView(DetailView):
    def get_data(self, request, *args, **kwargs):
        reference_name = kwargs['reference_name']
        return self.get_object().get_referential(reference_name, q=request.GET.get('q'))


class SuiviView(DetailView):
    def get_data(self, request, *args, **kwargs):
        suivi_type = kwargs['suivi_type']
        if suivi_type not in ('visite','plan-aide',
            'presentation-commission','decision-commission'):
            raise CommissionTypeNotFound(_('Unknown suivi type'))
        params = self._get_params(request, 'datedebut', 'datefin')
        return self.get_object().get_suivi(suivi_type, *params)


class IntegrationView(View, SingleObjectMixin):
    model = SolisAPA

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(IntegrationView, self).dispatch(request, *args, **kwargs)

    @utils.protected_api('can_access')
    @utils.to_json('api')
    def post(self, request, *args, **kwargs):
        data = request.body
        return self.get_object().import_flow(data)
