# passerelle.contrib.agoraplus
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required


from .views import *

urlpatterns = patterns('',
    url(r'^(?P<slug>[\w-]+)/$', AgoraPlusDetailView.as_view(),
        name='agoraplus-view'),
    url(r'^(?P<slug>[\w-]+)/ping/$', PingView.as_view(),
        name='agoraplus-ping'),

    url(r'^(?P<slug>[\w-]+)/family/link/$', LinkView.as_view(),
        name='agoraplus-family-link'),
    url(r'^(?P<slug>[\w-]+)/family/unlink/$', UnlinkView.as_view(),
        name='agoraplus-family-unlink'),
    url(r'^(?P<slug>[\w-]+)/family/$', FamilyView.as_view(),
        name='agoraplus-family'),
    url(r'^(?P<slug>[\w-]+)/family/(?P<key>[\w-]+)/$', FamilyItemView.as_view(),
        name='agoraplus-family-items'),
    url(r'^(?P<slug>[\w-]+)/family/(?P<key>[\w-]+)/(?P<item_id>[\w-]+)/$', FamilyItemView.as_view(),
        name='agoraplus-family-item'),

    url(r'^(?P<slug>[\w-]+)/sso/$', login_required(RedirectSSOView.as_view()),
        name='agoraplus-sso'),

    url(r'^(?P<slug>[\w-]+)/users/$', UserView.as_view(),
        name='agoraplus-user'),
    url(r'^(?P<slug>[\w-]+)/auth/$', AuthView.as_view(),
        name='agoraplus-auth'),
    url(r'^(?P<slug>[\w-]+)/phone/$', PhoneUpdateView.as_view(),
        name='agoraplus-phone-update'),
    url(r'^(?P<slug>[\w-]+)/profession/$', ProfessionUpdateView.as_view(),
        name='agoraplus-profession-update'),

    url(r'^(?P<slug>[\w-]+)/reference/(?P<name>[\w-]+)/((?P<reference_id>[\w-]+)/)?$',
        ReferenceView.as_view(), name='agoraplus-reference'),
    url(r'^(?P<slug>[\w-]+)/schools/((?P<school_id>[\w-]+)/)?$', SchoolView.as_view(),
        name='agoraplus-school'),
    url(r'^(?P<slug>[\w-]+)/educational-stages/((?P<edustage_id>[\w-]+)/)?$', EducationalStageView.as_view(),
        name='agoraplus-educational-stage'),

    url(r'^(?P<slug>[\w-]+)/address/communes/((?P<commune_id>[\w-]+)/)?$', CommuneView.as_view(),
        name='agoraplus-commune'),
    url(r'^(?P<slug>[\w-]+)/address/types-of-streets/((?P<type_of_street_id>[\w-]+)/)?$', TypeOfStreetView.as_view(),
        name='agoraplus-type-of-street'),
    url(r'^(?P<slug>[\w-]+)/address/communes/(?P<commune_id>[\w-]+)/types-of-streets/(?P<type_of_street_id>[\w-]+)/$', StreetView.as_view(),
        name='agoraplus-street-search'),
    url(r'^(?P<slug>[\w-]+)/address/streets/((?P<street_id>[\w-]+)/)?$', StreetView.as_view(),
        name='agoraplus-street'),
    url(r'^(?P<slug>[\w-]+)/address/?$', AddressUpdateView.as_view(),
        name='agoraplus-address-update'),

    url(r'^(?P<slug>[\w-]+)/exist/email/$', ExistEmailView.as_view(),
        name='agoraplus-exist-email'),
    url(r'^(?P<slug>[\w-]+)/exist/child/$', ExistChildView.as_view(),
        name='agoraplus-exist-child'),
    url(r'^(?P<slug>[\w-]+)/exist/representant/$', ExistRepresentantView.as_view(),
        name='agoraplus-exist-representant'),
    url(r'^(?P<slug>[\w-]+)/exist/list/$', ExistListView.as_view(),
        name='agoraplus-exist-list'),

    url(r'^(?P<slug>[\w-]+)/sas/purge/$', SasPurgeView.as_view(),
        name='agoraplus-sas-purge'),
    url(r'^(?P<slug>[\w-]+)/sas/family/$', SasFamilyView.as_view(),
        name='agoraplus-sas-family'),
    url(r'^(?P<slug>[\w-]+)/sas/check-duplicates/$', SasCheckDuplicatesView.as_view(),
        name='agoraplus-sas-check-duplicates'),
    url(r'^(?P<slug>[\w-]+)/sas/family/push/$', SasFamilyPushView.as_view(),
        name='agoraplus-sas-family-push'),
    url(r'^(?P<slug>[\w-]+)/sas/family/delete/$', SasPurgeView.as_view(),
        name='agoraplus-sas-family-delete'),
    url(r'^(?P<slug>[\w-]+)/sas/child/$', SasChildView.as_view(),
        name='agoraplus-sas-child'),
    url(r'^(?P<slug>[\w-]+)/sas/child/push/sas_(?P<sas_child_id>[\w-]+)/$', SasChildPushView.as_view(),
        name='agoraplus-sas-child-push'),
    url(r'^(?P<slug>[\w-]+)/sas/child/delete/sas_(?P<sas_child_id>[\w-]+)/$', SasChildDeleteView.as_view(),
        name='agoraplus-sas-child-delete'),
    url(r'^(?P<slug>[\w-]+)/sas/contact/push/sas_(?P<sas_contact_id>[\w-]+)/$', SasContactPushView.as_view(),
        name='agoraplus-sas-contact-push'),
    url(r'^(?P<slug>[\w-]+)/sas/contact/delete/sas_(?P<sas_contact_id>[\w-]+)/$', SasContactDeleteView.as_view(),
        name='agoraplus-sas-contact-delete'),
    url(r'^(?P<slug>[\w-]+)/sas/contact/$', SasContactView.as_view(),
        name='agoraplus-sas-contact'),
    url(r'^(?P<slug>[\w-]+)/incomes-declaration/?$', IncomesDeclarationView.as_view(),
        name='agoraplus-incomes-declaration'),
    url(r'^(?P<slug>[\w-]+)/school-enrollment/?$', SchoolEnrollmentView.as_view(),
        name='agoraplus-school-enrollment'),
    url(r'^(?P<slug>[\w-]+)/school-enrollments/?$', SchoolEnrollmentsView.as_view(),
        name='agoraplus-school-enrollments'),
    url(r'^(?P<slug>[\w-]+)/nursery-enrollment/?$', NurseryEnrollmentView.as_view(),
        name='agoraplus-nursery-enrollment'),
    url(r'^(?P<slug>[\w-]+)/nursery-enrollment-result/(?P<enroll_id>[\w-]+)/?$', NurseryEnrollmentResultView.as_view(),
        name='agoraplus-nursery-enrollment-result'),
    url(r'^(?P<slug>[\w-]+)/regie/invoices/$', InvoicesView.as_view(), name='agoraplus-invoices'),
    url(r'^(?P<slug>[\w-]+)/regie/invoices/history/$', HistoryInvoicesView.as_view(), name='agoraplus-invoices-history'),
    url(r'^(?P<slug>[\w-]+)/regie/invoice/(?P<invoice_id>[\w,-]+)/pay/$', InvoicePayView.as_view(), name='agoraplus-invoice-payment'),
    url(r'^(?P<slug>[\w-]+)/regie/invoice/(?P<invoice_id>[\w,-]+)/$', InvoiceView.as_view(), name='agoraplus-invoice'),
    url(r'^(?P<slug>[\w-]+)/regie/invoice/(?P<invoice_id>[\w,-]+)/pdf/$', InvoicePDFView.as_view(), name='agoraplus-invoice-pdf'),
    url(r'^(?P<slug>[\w-]+)/document/?$', DocumentView.as_view(), name='agoraplus-document'),
)
