# passerelle.contrib.agoraplus
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

AGE_LIMIT = 20

from datetime import datetime

def keywordize_child(child):
    child['keywords'] = child.get('keywords') or []
    if child.get('birthdate'):
        child['keywords'].append('enfant-annee-%s' % child['birthdate'][:4])
    if child.get('age'):
        age = int(child.get('age'))
        child['keywords'].append('enfant-age-%s' % age)
        for younger in range(0, age):
            child['keywords'].append('enfant-age-plus-de-%s' % younger)
        for older in range(age, AGE_LIMIT):
            child['keywords'].append('enfant-age-moins-de-%s' % (older+1))
    else:
        child['keywords'].append('enfant-age-inconnu')
    if child.get('school'):
        child['keywords'].append('enfant-inscrit')
        grade = child['school'].get('grade', {}).get('id', 'inconnu') or 'inconnu'
        child['keywords'].append('enfant-inscrit-niveau-%s' % grade)
        school = child['school'].get('id', 'inconnu') or 'inconnu'
        child['keywords'].append('enfant-inscrit-etablissement-%s' % school)
    else:
        child['keywords'].append('enfant-non-inscrit')
    return child

def keywordize_family(family):
    family['keywords'] = family.get('keywords') or []
    family['keywords'].append('famille-%d-adultes' % len(family['adults']))
    family['keywords'].append('famille-%d-enfants' % len(family['children']))
    family['keywords'].append('famille-%d-contacts' % len(family['contacts']))
    family['children'] = [keywordize_child(p) for p in family.get('children') or []]
    return family
