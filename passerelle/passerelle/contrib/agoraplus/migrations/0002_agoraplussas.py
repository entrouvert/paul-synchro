# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('agoraplus', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AgoraPlusSas',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name_id', models.CharField(max_length=256)),
                ('kind', models.CharField(max_length=16, choices=[(b'ADULT', b'ADULT'), (b'CHILD', b'CHILD'), (b'CONTACT', b'CONTACT')])),
                ('agoraplus_id', models.CharField(max_length=16, blank=True)),
                ('value', jsonfield.fields.JSONField(default=dict, blank=True)),
                ('resource', models.ForeignKey(to='agoraplus.AgoraPlus')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
