# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agoraplus', '0005_agoraplus_log_level'),
    ]

    operations = [
        migrations.AddField(
            model_name='agoraplus',
            name='frontoffice_url',
            field=models.CharField(max_length=128, verbose_name='Frontoffice URL (SSO)', blank=True),
        ),
    ]
