# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agoraplus', '0002_agoraplussas'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agoraplussas',
            name='kind',
            field=models.CharField(max_length=16, choices=[(b'FAMILY', b'FAMILY'), (b'CHILD', b'CHILD'), (b'CONTACT', b'CONTACT')]),
            preserve_default=True,
        ),
    ]
