# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AgoraPlus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50)),
                ('slug', models.SlugField()),
                ('description', models.TextField()),
                ('url', models.CharField(max_length=128, verbose_name='Webservices Base URL')),
                ('login', models.CharField(max_length=128, verbose_name='Auth login')),
                ('oauth_consumer_key', models.CharField(max_length=128, verbose_name='Auth oauth_consumer_key')),
                ('verify_cert', models.BooleanField(default=True, verbose_name='Check HTTPS Certificate validity')),
                ('username', models.CharField(max_length=128, verbose_name='HTTP Basic Auth username', blank=True)),
                ('password', models.CharField(max_length=128, verbose_name='HTTP Basic Auth password', blank=True)),
                ('keystore', models.FileField(help_text='Certificate and private key in PEM format', upload_to=b'agoraplus', null=True, verbose_name='Keystore', blank=True)),
                ('users', models.ManyToManyField(to='base.ApiUser', blank=True)),
            ],
            options={
                'verbose_name': 'Agora+',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AgoraPlusLink',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name_id', models.CharField(max_length=256)),
                ('login', models.CharField(max_length=128)),
                ('password', models.CharField(max_length=128)),
                ('resource', models.ForeignKey(to='agoraplus.AgoraPlus')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
