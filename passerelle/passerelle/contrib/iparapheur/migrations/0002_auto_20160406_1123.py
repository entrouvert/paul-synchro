# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('iparapheur', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Management',
            new_name='IParapheur',
        ),
    ]
