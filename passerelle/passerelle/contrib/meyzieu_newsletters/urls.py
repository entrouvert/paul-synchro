# Passerelle - uniform access to data and services
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import patterns, include, url

from .views import *

urlpatterns = patterns('',
    url(r'^(?P<slug>[\w,-]+)/$', ManagementDetailView.as_view(),
        name='meyzieu-newsletters-view'),
    url(r'^(?P<slug>[\w,-]+)/newsletters/$', NewslettersTypesView.as_view(),
        name='meyzieu-newsletters-types'),
    url(r'^(?P<slug>[\w,-]+)/subscriptions/$', NewslettersSubscriptionsView.as_view(),
        name='meyzieu-newsletters-subscriptions'),
)
