# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0002_auto_20151009_0326'),
    ]

    operations = [
        migrations.CreateModel(
            name='MeyzieuNewsletters',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50)),
                ('slug', models.SlugField()),
                ('description', models.TextField()),
                ('url', models.URLField(verbose_name='Newsletter service URL')),
                ('apikey', models.CharField(max_length=32, verbose_name='API key')),
                ('transport_titles_mapping', jsonfield.fields.JSONField(default=dict, verbose_name='Mapping of transports ids and titles')),
                ('users', models.ManyToManyField(to='base.ApiUser', blank=True)),
            ],
            options={
                'verbose_name': 'Meyzieu Newsletters',
            },
            bases=(models.Model,),
        ),
    ]
