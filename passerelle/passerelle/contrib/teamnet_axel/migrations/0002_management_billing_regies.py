# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('teamnet_axel', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='management',
            name='billing_regies',
            field=jsonfield.fields.JSONField(default=dict, verbose_name='Mapping between regie ids and billing ids'),
            preserve_default=True,
        ),
    ]
