# passerelle.contrib.teamnet_axel
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import patterns, include, url

from .views import *

urlpatterns = patterns('',
    url(r'^(?P<slug>[\w,-]+)/$', ManagementDetailView.as_view(),
        name='teamnet-axel-view'),
    url(r'^(?P<slug>[\w,-]+)/ping/$', PingView.as_view(),
        name='teamnet-axel-ping'),
    url(r'^(?P<slug>[\w,-]+)/auth/$', AuthView.as_view(),
        name='teamnet-axel-auth'),
    url(r'^(?P<slug>[\w,-]+)/family/link/$', LinkView.as_view(),
        name='teamnet-axel-link'),
    url(r'^(?P<slug>[\w,-]+)/family/unlink/$', UnlinkView.as_view(),
        name='teamnet-axel-unlink'),
    url(r'^(?P<slug>[\w,-]+)/family/$', FamilyView.as_view(),
        name='teamnet-axel-family'),
    url(r'^(?P<slug>[\w,-]+)/family/(?P<key>[\w,-]+)/$', FamilyView.as_view(),
        name='teamnet-axel-family-filtered'),
    url(r'^(?P<slug>[\w,-]+)/regie/(?P<regie_id>[\w,-]+)/invoices/history/$', HistoryInvoicesView.as_view(),
       name='teamnet-axel-invoices-history'),
    url(r'^(?P<slug>[\w,-]+)/regie/(?P<regie_id>[\w,-]+)/invoices/$', InvoicesView.as_view(),
       name='teamnet-axel-invoices'),
    url(r'^(?P<slug>[\w,-]+)/regie/(?P<regie_id>[\w,-]+)/invoice/(?P<invoice_id>[\w,-]+)/$', InvoiceView.as_view(),
        name='teamnet-axel-invoice'),
    url(r'^(?P<slug>[\w,-]+)/regie/(?P<regie_id>[\w,-]+)/invoice/(?P<invoice_id>[\w,-]+)/pdf/$', InvoicePDFView.as_view(),
        name='teamnet-axel-invoice-pdf'),
    url(r'^(?P<slug>[\w,-]+)/regie/(?P<regie_id>[\w,-]+)/invoice/(?P<invoice_id>[\w,-]+)/pay/$',
        InvoicePayView.as_view(),
        name='teamnet-axel-invoice-payment'),
)
