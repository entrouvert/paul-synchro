# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0002_auto_20151009_0326'),
        ('maarch', '0002_management_log_level'),
    ]

    operations = [
        migrations.RenameModel(
            'Management',
            'Maarch'
        ),
    ]
