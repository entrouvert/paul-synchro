# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('seisin_by_email', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='seisinbyemailmanagement',
            options={'verbose_name': 'Seisin by Email'},
        ),
    ]
