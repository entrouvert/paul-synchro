# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SeisinByEmailManagement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50)),
                ('slug', models.SlugField()),
                ('description', models.TextField()),
                ('wsdl_url', models.CharField(help_text='Seisin-by-Email WSDL URL', max_length=128, verbose_name='WSDL URL')),
                ('verify_cert', models.BooleanField(default=True, verbose_name='Check HTTPS Certificate validity')),
                ('username', models.CharField(max_length=128, verbose_name='Username', blank=True)),
                ('password', models.CharField(max_length=128, verbose_name='Password', blank=True)),
                ('keystore', models.FileField(help_text='Certificate and private key in PEM format', upload_to=b'seisin-by-email', null=True, verbose_name='Keystore', blank=True)),
                ('users', models.ManyToManyField(to='base.ApiUser', blank=True)),
            ],
            options={
                'verbose_name': 'Seisin-by-Email',
            },
            bases=(models.Model,),
        ),
    ]
