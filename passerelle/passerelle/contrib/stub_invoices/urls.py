# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2016 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.conf.urls import patterns, include, url

from .views import *


urlpatterns = patterns('',
    url(r'^(?P<slug>[\w,-]+)/invoices/history/$', HistoryInvoicesView.as_view(),
       name='stub-invoices-invoices-history'),
    url(r'^(?P<slug>[\w,-]+)/invoices/$', InvoicesView.as_view(),
       name='stub-invoices-invoices'),
    url(r'^(?P<slug>[\w,-]+)/invoice/(?P<invoice_id>[\w,-]+)/$', InvoiceView.as_view(),
        name='stub-invoices-invoice'),
    url(r'^(?P<slug>[\w,-]+)/invoice/(?P<invoice_id>[\w,-]+)/pdf/$', InvoicePDFView.as_view(),
        name='stub-invoices-invoice-pdf'),
    url(r'^(?P<slug>[\w,-]+)/invoice/(?P<invoice_id>[\w,-]+)/pay/$',
        InvoicePayView.as_view(),
        name='stub-invoices-invoice-payment'),
)
