# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2016 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
from decimal import Decimal
import random

from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

from passerelle.base.models import BaseResource


class StubInvoicesConnector(BaseResource):
    category = _('Stub Connectors')

    class Meta:
        verbose_name = _('Invoices')

    @classmethod
    def get_icon_class(cls):
        return 'ressources'

    # generate a serie of stub invoices
    invoices = {}
    for i in range(15):
        now = timezone.now()
        id_ = '%d%04d' % (now.year, i+1)
        invoices[id_] = {
            'id': id_,
            'display_id': id_,
            'total_amount': Decimal(random.randint(100, 10000)) / 100,
            'has_pdf': bool(i%3),
            'created': now - datetime.timedelta(days=20) + datetime.timedelta(days=i),
            'label': 'Label %s' % id_,
            'pay_limit_date': now + datetime.timedelta(days=2+random.randint(0, 10)),
            'online_payment': bool(i%2),
            'paid': False,
        }
        if i < 5:
            invoices[id_]['payment_date'] = invoices[id_]['created'] + datetime.timedelta(days=1+random.randint(0, 3))
            invoices[id_]['online_payment'] = False
            invoices[id_]['paid'] = True
        elif invoices[id_]['online_payment'] is False:
            invoices[id_]['no_online_payment_reason'] = random.choice(['autobilling', 'litigation'])

        invoices[id_]['amount'] = invoices[id_]['total_amount']

    def get_invoices(self):
        return self.invoices.values()

    def get_invoice(self, invoice_id):
        return self.invoices.get(invoice_id)
