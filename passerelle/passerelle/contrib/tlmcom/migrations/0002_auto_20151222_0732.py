# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tlmcom', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='tlmcom',
            options={'verbose_name': 'TLM COM PBX', 'verbose_name_plural': 'TLM COM PBX'},
        ),
    ]
