# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import passerelle.pbx.models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0002_auto_20151009_0326'),
    ]

    operations = [
        migrations.CreateModel(
            name='TlmCom',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50)),
                ('slug', models.SlugField()),
                ('description', models.TextField()),
                ('welco_url', models.URLField(default=passerelle.pbx.models.get_default_welco_url, max_length=120, verbose_name='welco URL')),
                ('users', models.ManyToManyField(to='base.ApiUser', blank=True)),
            ],
            options={
                'verbose_name': 'TLM COM gateway',
                'verbose_name_plural': 'TLM COM gateways',
            },
            bases=(models.Model,),
        ),
    ]
