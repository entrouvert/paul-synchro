from passerelle.pbx.models import PBXMixin

from django.utils.translation import ugettext_lazy as _


class TlmCom(PBXMixin):
    class Meta:
        verbose_name = _('TLM COM PBX')
        verbose_name_plural = _('TLM COM PBX')
