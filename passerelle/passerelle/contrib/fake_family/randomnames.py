# -*- encoding: utf-8 -*-

import random

ADDRESS = ['Allée Alexandre Vialatte', 'Allée André Breton', 'Allée Blaise Cendrars',
        'Allée Darius Milhaud', 'Avenue Bosquet', 'Avenue Carnot',
        'Avenue Aimé Césaire', 'Avenue Daumesnil', 'Avenue des Sycomores',
        'Avenue des Terroirs de France', 'Avenue des Tilleuls', 'Avenue d\'Italie',
        'Avenue Dode de la Brunerie', 'Avenue Marceau', 'Avenue Molière',
        'Avenue Montaigne', 'Avenue Mozart', 'Boulevard Auguste Blanqui',
        'Boulevard Barbès', 'Boulevard Beaumarchais', 'Boulevard d\'Algérie',
        'Boulevard d\'Aurelle de Paladines', 'Boulevard de Reims',
        'Boulevard de Reuilly', 'Boulevard de Rochechouart',
        'Boulevard des Batignolles', 'Carrefour de l\'Odéon', 'Chaussée de l\'Étang',
        'Chemin du Parc de Charonne', 'Cité Charles Godon', 'Cité Condorcet',
        'Cité d\'Angoulême', 'Cour Saint-Émilion', 'Cour Saint-Pierre',
        'Cours des Maréchaux', 'Esplanade Nathalie Sarraute',
        'Esplanade Pierre Vidal-Naquet', 'Galerie Colbert', 'Galerie de Beaujolais',
        'Galerie de la Villette', 'Galerie des Variétés', 'Hameau Béranger',
        'Hameau d\'Alleray', 'Impasse Beaubourg', 'Impasse Bonne Nouvelle',
        'Impasse Bon Secours', 'Impasse Chausson', 'Impasse d\'Amsterdam',
        'Impasse de la Santé', 'Impasse de la Tour d\'Auvergne', 'Impasse Delepine',
        'Impasse des 2 Anges', 'Impasse des 2 Cousins', 'Impasse des 3 Soeurs',
        'Parvis du Sacré-Coeur', 'Passage Abel Leblanc', 'Passage Alexandre',
        'Passage Alexandrine', 'Passage Alombert', 'Passage des Charbonniers',
        'Passage des Crayons', 'Place André Malraux', 'Place Balard', 'Place Bienvenue',
        'Place Blanche', 'Place Cambronne', 'Place Carrée', 'Place de la Porte de Saint-Cloud',
        'Place de la Porte de Vanves', 'Place de la Porte Maillot', 'Place de la Porte Molitor',
        'Place de la République', 'Place de la Sorbonne', 'Port de Javel Haut',
        'Port de la Bourdonnais', 'Port de la Concorde', 'Quai de Conti',
        'Quai de Gesvres', 'Quai de Grenelle', 'Quai de Jemmapes', 'Route de Bourbon',
        'Route de Ceinture du Lac Daumesnil', 'Route de la Brasserie', 'Route de la Cascade',
        'Route de la Croix Rouge', 'Rue Calmels Prolongée', 'Rue Cambacérès',
        'Rue Cambon', 'Rue Cambronne', 'Rue Camille Blaisot', 'Rue de Bretonvilliers',
        'Rue de Brissac', 'Rue de Brosse', 'Rue Debrousse', 'Rue de Bruxelles',
        'Rue de Bucarest', 'Rue de Buci', 'Rue de Budapest', 'Rue de Buenos Ayres',
        'Rue de l\'Annonciation', 'Rue de la Nouvelle-Calédonie', 'Rue de la Paix',
        'Rue de la Parcheminerie', 'Rue de la Pépinière', 'Rue Jean Giono',
        'Rue Jean Giraudoux', 'Rue Jean Goujon', 'Rue Jean Hugues',
        'Rue Jean-Jacques Rousseau', 'Rue Jean Lantier', 'Rue René Bazin',
        'Rue René Clair', 'Rue René Goscinny', 'Rue Riblette', 'Rue Riboutte',
        'Square Alboni', 'Square Alfred Capus', 'Square Alfred Dehodencq',
        'Square Amicie Lebaudy', 'Square André Dreyer', 'Square André Lichtenberger',
        'Villa Austerlitz', 'Villa Ballu', 'Villa Baumann', 'Villa Belliard',
        'Voie Communale', 'Voie Georges Pompidou', 'Voie Mazas']

LASTNAME = ['MARTIN', 'BERNARD', 'ROUX', 'THOMAS', 'PETIT', 'DURAND', 'MICHEL',
        'ROBERT', 'RICHARD', 'SIMON', 'MOREAU', 'DUBOIS', 'BLANC', 'LAURENT', 'GIRARD',
        'BERTRAND', 'GARNIER', 'DAVID', 'MOREL', 'GUERIN', 'FOURNIER', 'ROY',
        'ROUSSEAU', 'ANDRE', 'GAUTIER', 'BONNET', 'LAMBERT', 'HENRY', 'FAURE',
        'MERCIER', 'VINCENT', 'CHEVALIER', 'LEROY', 'MARCHAND', 'PERRIN',
        'MORIN', 'MASSON', 'GIRAUD', 'DUPONT', 'ROBIN', 'NICOLAS', 'BRUN',
        'MATHIEU', 'CLEMENT', 'LEFEBVRE', 'FABRE', 'BARBIER', 'FRANCOIS',
        'ROUSSEL', 'ARNAUD', 'GERARD', 'AUBERT', 'DUVAL', 'LEGRAND',
        'BLANCHARD', 'BRUNET', 'LEFEVRE', 'DENIS', 'BRETON', 'PIERRE', 'ROCHE',
        'PARIS', 'BOYER', 'COLIN', 'FONTAINE', 'JEAN', 'BOURGEOIS', 'GAILLARD',
        'NOEL', 'DUMAS', 'PICARD', 'BRIAND', 'LUCAS', 'ROLLAND', 'JOLY']

FIRSTNAME = {
        'child': {
            'M': ['Adam', 'Alex', 'Alexandre', 'Alexis', 'Anthony', 'Antoine',
                'Benjamin', 'Cédric', 'Charles', 'Christopher', 'David',
                'Dylan', 'Édouard', 'Elliot', 'Émile', 'Étienne', 'Félix',
                'Gabriel', 'Guillaume', 'Hugo', 'Isaac', 'Jacob', 'Jérémy',
                'Jonathan', 'Julien', 'Justin', 'Léo', 'Logan', 'Loïc',
                'Louis', 'Lucas', 'Ludovic', 'Malik', 'Mathieu', 'Mathis',
                'Maxime', 'Michaël', 'Nathan', 'Nicolas', 'Noah', 'Olivier',
                'Philippe', 'Raphaël', 'Samuel', 'Simon', 'Thomas', 'Tommy',
                'Tristan', 'Victor', 'Vincent'],
            'F': ['Alexia', 'Alice', 'Alicia', 'Amélie', 'Anaïs', 'Annabelle',
                'Arianne', 'Audrey', 'Aurélie', 'Camille', 'Catherine',
                'Charlotte', 'Chloé', 'Clara', 'Coralie', 'Daphnée',
                'Delphine', 'Elizabeth', 'Élodie', 'Émilie', 'Emma', 'Emy',
                'Ève', 'Florence', 'Gabrielle', 'Jade', 'Juliette', 'Justine',
                'Laurence', 'Laurie', 'Léa', 'Léanne', 'Maélie', 'Maéva',
                'Maika', 'Marianne', 'Marilou', 'Maude', 'Maya', 'Mégan',
                'Mélodie', 'Mia', 'Noémie', 'Océane', 'Olivia', 'Rosalie',
                'Rose', 'Sarah', 'Sofia', 'Victoria']
            },
        'adult': {
            'M': ['Jean', 'Philippe', 'Michel', 'Alain', 'Patrick', 'Nicolas',
                'Christophe', 'Pierre', 'Christian', 'Éric', 'Frédéric',
                'Laurent', 'Stéphane', 'David', 'Pascal', 'Daniel',
                'Sébastien', 'Julien', 'Thierry', 'Olivier', 'Bernard',
                'Thomas', 'Alexandre', 'Gérard', 'Didier', 'Dominique',
                'Vincent', 'François', 'Bruno', 'Guillaume', 'Jérôme',
                'Jacques', 'Marc', 'Maxime', 'Romain', 'Claude', 'Antoine',
                'Franck', 'Jean-Pierre', 'Anthony', 'Kévin', 'Gilles',
                'Cédric', 'Serge', 'André', 'Mathieu', 'Benjamin', 'Patrice',
                'Fabrice', 'Joël', 'Jérémy', 'Clément', 'Arnaud', 'Denis',
                'Paul', 'Lucas', 'Hervé', 'Jean-Claude', 'Sylvain', 'Yves',
                'Ludovic', 'Guy', 'Florian', 'Damien', 'Alexis', 'Mickaël',
                'Quentin', 'Emmanuel', 'Louis', 'Benoît', 'Jean-Luc', 'Fabien',
                'Francis', 'Hugo', 'Jonathan', 'Loïc', 'Xavier', 'Théo',
                'Adrien', 'Raphaël', 'Jean-Francois', 'Grégory', 'Robert',
                'Michaël', 'Valentin', 'Cyril', 'Jean-Marc', 'René', 'Lionel',
                'Yannick', 'Enzo', 'Yann', 'Jean-Michel', 'Baptiste',
                'Matthieu', 'Rémi', 'Georges', 'Aurélien', 'Nathan',
                'Jean-Paul'],
            'F': ['Marie', 'Nathalie', 'Isabelle', 'Sylvie', 'Catherine',
                'Martine', 'Christine', 'Françoise', 'Valerie', 'Sandrine',
                'Stephanie', 'Veronique', 'Sophie', 'Celine', 'Chantal',
                'Patricia', 'Anne', 'Brigitte', 'Julie', 'Monique', 'Aurelie',
                'Nicole', 'Laurence', 'Annie', 'Émilie', 'Dominique',
                'Virginie', 'Corinne', 'Elodie', 'Christelle', 'Camille',
                'Caroline', 'Lea', 'Sarah', 'Florence', 'Laetitia', 'Audrey',
                'Helene', 'Laura', 'Manon', 'Michele', 'Cecile', 'Christiane',
                'Beatrice', 'Claire', 'Nadine', 'Delphine', 'Pauline',
                'Karine', 'Melanie', 'Marion', 'Chloe', 'Jacqueline',
                'Elisabeth', 'Evelyne', 'Marine', 'Claudine', 'Anais', 'Lucie',
                'Danielle', 'Carole', 'Fabienne', 'Mathilde', 'Sandra',
                'Pascale', 'Annick', 'Charlotte', 'Emma', 'Severine',
                'Sabrina', 'Amandine', 'Myriam', 'Jocelyne', 'Alexandra',
                'Angelique', 'Josiane', 'Joelle', 'Agnes', 'Mireille',
                'Vanessa', 'Justine', 'Sonia', 'Bernadette', 'Emmanuelle',
                'Oceane', 'Amelie', 'Clara', 'Maryse', 'Anne-marie', 'Fanny',
                'Magali', 'Marie-christine', 'Morgane', 'Ines', 'Nadia',
                'Muriel', 'Jessica', 'Laure', 'Genevieve', 'Estelle']
            }
        }


def person(kind='child'):
    sex = random.choice(FIRSTNAME[kind].keys())
    first_name = random.choice(FIRSTNAME[kind][sex])
    last_name = random.choice(LASTNAME)
    return {
        'sex': sex,
        'first_name': first_name,
        'last_name': last_name
    }

def address():
    return {
        'address': '%d %s' % (random.randint(1,100), random.choice(ADDRESS)),
        'city': 'ConnectVille',
        'post_code': '75014',
    }
