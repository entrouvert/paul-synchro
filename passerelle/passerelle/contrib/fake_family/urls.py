# passerelle.contrib.fake_family
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import patterns, include, url

from .views import *

urlpatterns = patterns('',
    url(r'^(?P<slug>[\w,-]+)/$', FakeFamilyDetailView.as_view(),
        name='fake-family-view'),
    url(r'^(?P<slug>[\w,-]+)/dump/$', DumpView.as_view(),
        name='fake-family-dump'),
    url(r'^(?P<slug>[\w,-]+)/family/link/$', LinkView.as_view(),
        name='fake-family-link'),
    url(r'^(?P<slug>[\w,-]+)/family/unlink/$', UnlinkView.as_view(),
        name='fake-family-unlink'),
    url(r'^(?P<slug>[\w,-]+)/family/$', FamilyView.as_view(),
        name='fake-family-info'),
    url(r'^(?P<slug>[\w,-]+)/family/(?P<key>[\w,-]+)/$', FamilyKeyView.as_view(),
        name='fake-family-key'),
)
