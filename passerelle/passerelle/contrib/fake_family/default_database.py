# passerelle.contrib.fake_family
# -*- encoding: utf-8 -*-
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime, timedelta
import random
import randomnames
import json


def default_database():

    now = datetime.now()

    invoices = {}
    for i in range(10):
        day = now + timedelta(15*(i-6)-1)
        limit = day + timedelta(30)
        total_amount = '%.2f' % (random.randint(1000, 5000) / 100.0)
        if i > 5:
            amount = total_amount
        else:
            amount = "0.00" # paid
        invoices["F%d%0.2d-%d" % (day.year, day.month, random.randint(100,999))] = {
            "amount": amount,
            "label": "facture du %d/%d/%d" % (day.day, day.month, day.year),
            "total_amount": total_amount,
            "online_payment": i%3 != 0,
            "created": day.strftime('%Y-%m-%dT%H:%M:%S'),
            "pay_limit_date": limit.strftime('%Y-%m-%dT%H:%M:%S'),
            "has_pdf": True,
            "keywords": [],
        }

    adults = {}
    for i in range(1, 22, 2):
        adult = randomnames.person('adult')
        adult.update(randomnames.address())
        adult['login'] = 'p%d@example.net' % i
        adult['password'] = 'pass%d' % i
        adult['email'] = 'p%d@example.net' % i
        adult['text'] = '%(first_name)s %(last_name)s' % adult
        adult['birthdate'] = "%d-%0.2d-18" % (now.year-random.randint(20,40), random.randint(1,12))
        adult['phone'] = '0122334455'
        adult['cellphone'] = '0655443322'
        adult['invoices'] = []
        adult['keywords'] = []
        adult['id'] = i
        adults['%d' % i] = adult
        adult2 = adult.copy()
        adult2.update(randomnames.person('adult'))
        adult2['last_name'] = adult['last_name']
        adult2['text'] = '%(first_name)s %(last_name)s' % adult2
        adult2['birthdate'] = "%d-%0.2d-18" % (now.year-random.randint(20,40), random.randint(1,12))
        adult2['login'] = 'p%d@example.net' % (i+1)
        adult2['password'] = 'pass%d' % (i+1)
        adult2['email'] = 'p%d@example.net' % (i+1)
        adult2['id'] = i+1
        adults['%d' % (i+1)] = adult2

    children = {}
    for i in range(1,51):
        child = randomnames.person('child')
        child['text'] = '%(first_name)s %(last_name)s' % child
        child['birthdate'] = "%d-%0.2d-18" % (now.year-random.randint(1,14), random.randint(1,12))
        child['keywords'] = ["naissance-en-"+child['birthdate'][:4]]
        child['id'] = i
        children['%d' % i] = child

    database = {
        "links": {
            # "nameid": "adult_id",
            "fake_nameid": "1",
        },
        "adults": adults,
        "children": children,
        "invoices": invoices,
        "families": {
            "1": {
                "id": "1",
                "adults": ["1", "2"],
                "children": ["1", "2"],
                "contacts": ["10"],
                "invoices": [],
                "keywords": ["deux-enfants", "habitant-14eme"],
            },
            "2": {
                "id": "2",
                "adults": ["3"],
                "children": ["3", "4"],
                "contacts": [],
                "invoices": [],
                "keywords": ["deux-enfants", "habitant-14eme", "mono-parentale"],
            },
            "3": {
                "id": "3",
                "adults": ["5", "6"],
                "children": ["5"],
                "contacts": [],
                "invoices": [],
                "keywords": ["un-enfant", "habitant-14eme"],
            },
            "4": {
                "id": "4",
                "adults": ["7"],
                "children": ["6"],
                "contacts": [],
                "invoices": [],
                "keywords": ["un-enfant", "habitant-14eme", "mono-parentale"],
            },
            "5": {
                "id": "5",
                "adults": ["9", "10"],
                "children": [],
                "contacts": ["15"],
                "invoices": [],
                "keywords": ["sans-enfant", "habitant-14eme"],
            },
        },
    }

    return json.loads(json.dumps(database))
