from django.conf import settings
from django.conf.urls import patterns, include, url

from django.contrib import admin

from django.contrib.auth.decorators import login_required
from django.views.static import serve as static_serve

from .views import (HomePageView, ManageView, ManageAddView,
        GenericCreateConnectorView, GenericDeleteConnectorView,
        GenericEditConnectorView, GenericEndpointView, GenericConnectorView,
        LEGACY_APPS_PATTERNS, LegacyPageView, login, logout, menu_json)
from .urls_utils import decorated_includes, required, app_enabled, manager_required
from .base.urls import access_urlpatterns
from .plugins import register_apps_urls

import choosit.urls
import pastell.urls

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', HomePageView.as_view(), name='homepage'),
    url(r'^legacy/$', LegacyPageView.as_view(), name='legacy'),

    url(r'^manage/$', manager_required(ManageView.as_view()), name='manage-home'),
    url(r'^manage/menu.json$', manager_required(menu_json), name='menu-json'),
    url(r'^manage/add$', manager_required(ManageAddView.as_view()), name='add-connector'),

    url(r'^media/(?P<path>.*)$', login_required(static_serve), {
        'document_root': settings.MEDIA_ROOT,
    }),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^manage/access/',
        decorated_includes(manager_required, include(access_urlpatterns))),
)

urlpatterns += required(
    app_enabled('choosit'),
    patterns('',
        url(r'^choosit/', include(choosit.urls.urlpatterns)),
        url(r'^manage/choosit/',
            decorated_includes(manager_required, include(choosit.urls.management_urlpatterns))),
    )
)

urlpatterns += required(
    app_enabled('pastell'),
    patterns('',
        url(r'^manage/pastell/',
            decorated_includes(manager_required, include(pastell.urls.management_urlpatterns))),
    )
)

# add patterns from apps
urlpatterns = register_apps_urls(urlpatterns)

# add authentication patterns
urlpatterns += patterns('',
    url(r'^logout/$', logout, name='logout'),
    url(r'^login/$', login, name='auth_login'),
)

if 'mellon' in settings.INSTALLED_APPS:
    urlpatterns += patterns('', url(r'^accounts/mellon/', include('mellon.urls')))


# activate URL for installed apps only
for app, d in LEGACY_APPS_PATTERNS.items():
    if 'passerelle.%s' % app in settings.INSTALLED_APPS:
        urlpatterns += patterns('',
            url(r'^%s/' % d['url'], include('passerelle.%s.urls' % app),
                {'resource_name': d['name']}),
        )

urlpatterns += patterns('',
    url(r'^manage/(?P<connector>[\w,-]+)/', decorated_includes(manager_required,
        include(patterns('',
            url(r'^add$',
                GenericCreateConnectorView.as_view(), name='create-connector'),
            url(r'^(?P<slug>[\w,-]+)/delete$',
                GenericDeleteConnectorView.as_view(), name='delete-connector'),
            url(r'^(?P<slug>[\w,-]+)/edit$',
                GenericEditConnectorView.as_view(), name='edit-connector'),
        )))))

urlpatterns += patterns('',
    url(r'^(?P<connector>[\w,-]+)/(?P<slug>[\w,-]+)/$',
        GenericConnectorView.as_view(), name='view-connector'),
    url(r'^(?P<connector>[\w,-]+)/(?P<slug>[\w,-]+)/(?P<endpoint>[\w,-]+)(?:/(?P<rest>.*))?$',
        GenericEndpointView.as_view(), name='generic-endpoint')
)

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns += staticfiles_urlpatterns()
