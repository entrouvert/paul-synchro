# -*- coding: utf-8 -*-

from django.conf import settings
from .models import TemplateVar

def template_vars(request):
    """
    returns a dict with all the template vars, build from settings and database
    """
    vars = {}
    if hasattr(settings, 'TEMPLATE_VARS'):
        vars.update(settings.TEMPLATE_VARS)
    vars.update(dict((obj.name, obj.value) for obj in TemplateVar.objects.all()))

    return vars
