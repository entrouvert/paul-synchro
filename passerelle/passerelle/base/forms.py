from django import forms

from .models import ApiUser, AccessRight

class ApiUserForm(forms.ModelForm):
    class Meta:
        model = ApiUser
        exclude = []


class AccessRightForm(forms.ModelForm):
    class Meta:
        model = AccessRight
        exclude = []
        widgets = {
            'codename': forms.HiddenInput(),
            'resource_type': forms.HiddenInput(),
            'resource_pk': forms.HiddenInput(),
        }
