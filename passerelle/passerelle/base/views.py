from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.views.generic import *
from django.http import Http404

from .models import ApiUser, AccessRight
from .forms import ApiUserForm, AccessRightForm
from ..utils import get_trusted_services

class ResourceIndexView(ListView):
    template_name = 'passerelle/base/index.html'

    def get_queryset(self):
        if self.request.user.is_superuser:
            qs = self.model.objects.all()
        else:
            qs = self.model.objects.filter_apiuser(self.request.apiuser)
        return qs


class ResourceView(DetailView):
    template_name = 'passerelle/base/view.html'

    def get_object(self, queryset=None):
        try:
            obj = self.model.objects.get_subclass(slug=self.kwargs['slug'])
        except ObjectDoesNotExist:
            raise Http404
        if obj.is_accessible_by(self.request):
            return obj
        raise PermissionDenied

    def get_context_data(self, slug=None, **kwargs):
        context = super(ResourceView, self).get_context_data(**kwargs)
        context['site_base_uri'] = '%s://%s' % (
                'https' if self.request.is_secure() else 'http',
                self.request.get_host())
        context['absolute_uri'] = '%s%s' % (
                context['site_base_uri'],
                self.request.path)
        return context


class ApiUserCreateView(CreateView):
    model = ApiUser
    form_class = ApiUserForm
    template_name = 'passerelle/manage/apiuser_form.html'

    def get_success_url(self):
        return reverse('apiuser-list')


class ApiUserUpdateView(UpdateView):
    model = ApiUser
    form_class = ApiUserForm
    template_name = 'passerelle/manage/apiuser_form.html'

    def get_success_url(self):
        return reverse('apiuser-list')


class ApiUserDeleteView(DeleteView):
    model = ApiUser
    template_name = 'passerelle/manage/apiuser_confirm_delete.html'

    def get_success_url(self):
        return reverse('apiuser-list')


class ApiUserListView(ListView):
    model = ApiUser
    template_name = 'passerelle/manage/apiuser_list.html'
    paginate_by = 25

    def get_context_data(self, slug=None, **kwargs):
        context = super(ApiUserListView, self).get_context_data(**kwargs)
        context['trusted_services'] = get_trusted_services()
        return context


class AccessRightDeleteView(DeleteView):
    model = AccessRight
    template_name = 'passerelle/manage/accessright_confirm_delete.html'

    def get_object(self):
        object = super(AccessRightDeleteView, self).get_object()
        self.resource = object.resource
        return object

    def get_success_url(self):
        return self.resource.get_absolute_url()


class AccessRightCreateView(CreateView):
    model = AccessRight
    form_class = AccessRightForm
    template_name = 'passerelle/manage/accessright_form.html'

    def get_initial(self):
        d = self.initial.copy()
        d['codename'] = self.kwargs.get('codename')
        d['resource_type'] = self.kwargs.get('resource_type')
        d['resource_pk'] = self.kwargs.get('resource_pk')
        return d

    def get_success_url(self):
        return self.object.resource.get_absolute_url()
