from django.conf.urls import patterns, url

from .views import ApiUserCreateView, ApiUserUpdateView, ApiUserDeleteView, \
        ApiUserListView, AccessRightDeleteView, AccessRightCreateView

access_urlpatterns = patterns('',
    url(r'^$', ApiUserListView.as_view(), name='apiuser-list'),
    url(r'^add$', ApiUserCreateView.as_view(), name='apiuser-add'),
    url(r'^(?P<pk>[\w,-]+)/edit$', ApiUserUpdateView.as_view(), name='apiuser-edit'),
    url(r'^(?P<pk>[\w,-]+)/delete$', ApiUserDeleteView.as_view(), name='apiuser-delete'),

    url(r'^(?P<pk>[\w,-]+)/remove$', AccessRightDeleteView.as_view(),
        name='access-right-remove'),
    url(r'^accessright/add/(?P<resource_type>[\w,-]+)/(?P<resource_pk>[\w,-]+)/(?P<codename>[\w,-]+)/',
        AccessRightCreateView.as_view(), name='access-right-add')
)
