from django.contrib import admin

from .models import ApiUser, TemplateVar, AccessRight

admin.site.register(ApiUser)
admin.site.register(TemplateVar)
admin.site.register(AccessRight)
