# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BaseDataSource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50)),
                ('slug', models.SlugField()),
                ('description', models.TextField()),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CsvDataSource',
            fields=[
                ('basedatasource_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='datasources.BaseDataSource')),
                ('csv_file', models.FileField(upload_to=b'csv')),
                ('key_column', models.IntegerField()),
                ('value_columns', models.CommaSeparatedIntegerField(max_length=100)),
            ],
            options={
                'abstract': False,
            },
            bases=('datasources.basedatasource',),
        ),
        migrations.AddField(
            model_name='basedatasource',
            name='users',
            field=models.ManyToManyField(to='base.ApiUser', blank=True),
            preserve_default=True,
        ),
    ]
