from django.conf.urls import patterns, url, include

from ..base.views import ResourceIndexView, ResourceView
from .models import BaseDataSource

urlpatterns = patterns('passerelle.datasources.views',
    # human views
    url(r'^$', ResourceIndexView.as_view(model=BaseDataSource)),
    url(r'^(?P<slug>[\w-]+)/$', ResourceView.as_view(model=BaseDataSource,
        template_name='datasources/view.html')),
    # technical views (JSON)
    url(r'^(?P<slug>[\w-]+)/(?P<id>[^/]+)/json$', 'json_view'),
    url(r'^(?P<slug>[\w-]+)/json$', 'json_view'),
    url(r'^(?P<slug>[\w-]+)/jsonp$', 'jsonp'),
)
