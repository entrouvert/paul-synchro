from django.contrib import admin

from models import CsvDataSource

class CsvDataSourceAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(CsvDataSource, CsvDataSourceAdmin)
