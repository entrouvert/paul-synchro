DEBUG = True

SECRET_KEY = 'changeme'

MEDIA_ROOT = 'media'

STATIC_ROOT = 'collected-static'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME':  'passerelle.sqlite3',
    }
}
