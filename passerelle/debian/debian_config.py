# This file is sourced by "execfile" from passerelle.settings

import os

# Debian defaults
DEBUG = False

PROJECT_NAME = 'passerelle'

# SAML2 authentication
INSTALLED_APPS += ('mellon',)

#
# hobotization (multitenant)
#
execfile('/usr/lib/hobo/debian_config_common.py')

# disable django-mellon autologin
MELLON_OPENED_SESSION_COOKIE_NAME = None

# suds logs are buggy
LOGGING['loggers']['suds'] = {
        'level': 'ERROR',
        'handlers': ['mail_admins', 'sentry'],
        'propagate': True,
}

execfile('/etc/%s/settings.py' % PROJECT_NAME)
