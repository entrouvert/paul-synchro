#!/bin/sh

set -e -x
env
if [ -f /var/lib/jenkins/pylint.django.rc ]; then
	PYLINT_RC=/var/lib/jenkins/pylint.django.rc
elif [ -f pylint.django.rc ]; then
	PYLINT_RC=pylint.django.rc
else
	echo No pylint RC found
	exit 0
fi

test -f pylint.out && cp pylint.out pylint.out.prev
pylint -f parseable --rcfile ${PYLINT_RC} "$@" | tee pylint.out || /bin/true
test -f pylint.out.prev && (diff pylint.out.prev pylint.out | grep '^[><]' | grep .py) || /bin/true¶
