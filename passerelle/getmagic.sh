#!/bin/sh

# Get venv site-packages path
DSTDIR=`python -c 'from distutils.sysconfig import get_python_lib; print(get_python_lib())'`

# Get not venv site-packages path
# Remove first path (assuming that is the venv path)
NONPATH=`echo $PATH | sed 's/^[^:]*://'`
if ! /usr/bin/python -c 'import magic' >/dev/null 2>&1; then
	echo magic package is not installed
	exit 1
fi
SRCDIR=`/usr/bin/python -c 'from distutils.sysconfig import get_python_lib; print(get_python_lib())'`

# Clean up
rm -f $DSTDIR/magic.py

# Link
ln -sv $SRCDIR/magic.py $DSTDIR

exit 0

