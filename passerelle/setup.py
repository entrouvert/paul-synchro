#!/usr/bin/python

import os
import subprocess
import sys

from setuptools.command.install_lib import install_lib as _install_lib
from setuptools.command.sdist import sdist
from distutils.command.build import build as _build
from distutils.cmd import Command
from setuptools import setup, find_packages

def get_version():
    if os.path.exists('VERSION'):
        version_file = open('VERSION', 'r')
        version = version_file.read()
        version_file.close()
        return version
    if os.path.exists('.git'):
        p = subprocess.Popen(['git','describe','--match=v*'], stdout=subprocess.PIPE)
        result = p.communicate()[0]
        version = result.split()[0][1:]
        version = version.replace('-', '.')
        return version
    return '0'

class eo_sdist(sdist):

    def run(self):
        print "creating VERSION file"
        if os.path.exists('VERSION'):
            os.remove('VERSION')
        version = get_version()
        version_file = open('VERSION', 'w')
        version_file.write(version)
        version_file.close()
        sdist.run(self)
        print "removing VERSION file"
        if os.path.exists('VERSION'):
            os.remove('VERSION')

class compile_translations(Command):
    description = 'compile message catalogs to MO files via django compilemessages'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        try:
            from django.core.management import call_command
            for path, dirs, files in os.walk('passerelle'):
                if 'locale' not in dirs:
                    continue
                curdir = os.getcwd()
                os.chdir(os.path.realpath(path))
                call_command('compilemessages')
                os.chdir(curdir)
        except ImportError:
            sys.stderr.write('!!! Please install Django >= 1.4 to build translations\n')


class build(_build):
    sub_commands = [('compile_translations', None)] + _build.sub_commands


class install_lib(_install_lib):
    def run(self):
        self.run_command('compile_translations')
        _install_lib.run(self)


setup(name='passerelle',
        version=get_version(),
        license='AGPLv3',
        description='Passerelle provides an uniform access to multiple data sources and services.',
        url='https://dev.entrouvert.org/projects/passerelle/',
        download_url='http://repos.entrouvert.org/passerelle.git/',
        author="Entr'ouvert",
        author_email="info@entrouvert.com",
        packages=find_packages(os.path.dirname(__file__) or '.'),
        scripts=['manage.py'],
        include_package_data=True,
        install_requires=[
            'django >= 1.7, <1.9',
            'django-model-utils',
            'django-jsonfield < 1.0.0',
            'requests',
            'gadjo',
            'phpserialize',
            'suds',
            'SOAPpy',
            'pyexcel-io',
            'pyexcel-ods',
            'pyexcel-xls',
        ],
        cmdclass={
            'build': build,
            'compile_translations': compile_translations,
            'install_lib': install_lib,
            'sdist': eo_sdist,
        },
)
