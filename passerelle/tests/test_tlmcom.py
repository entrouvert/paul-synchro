import json
import pytest
import mock

from django.core.urlresolvers import reverse

from passerelle.contrib.tlmcom.models import TlmCom

pytestmark = pytest.mark.django_db


@mock.patch('requests.post')
def test_call_start(requests_post, client):
    URL = 'http://welco.example.net/'
    TlmCom.objects.create(slug='tlmcom', description='tlmcom', welco_url=URL)
    url = reverse('tlmcom-call-start', kwargs={'slug': 'tlmcom'}) \
        + '?caller=0033699999999&callee=102&user=zozo'
    response = client.get(url)
    assert response.status_code == 200
    assert response.content == 'ok'
    assert requests_post.call_count == 1

    assert requests_post.call_args == mock.call(URL, data=mock.ANY, headers={'content-type': 'application/json'})
    payload = json.loads(requests_post.call_args[1]['data'])
    assert isinstance(payload, dict)
    assert set(payload.keys()) == set(['data', 'caller', 'callee', 'event'])
    assert payload['event'] == 'start'
    assert payload['data'] == {'user': 'zozo'}
    assert payload['callee'] == '102'
    assert payload['caller'] == '0033699999999'

@mock.patch('requests.post')
def test_call_stop(requests_post, client):
    URL = 'http://welco.example.net/'
    TlmCom.objects.create(slug='tlmcom', description='tlmcom', welco_url=URL)
    url = reverse('tlmcom-call-stop', kwargs={'slug': 'tlmcom'}) \
        + '?caller=0033699999999&callee=102&user=zozo'
    response = client.get(url)
    assert response.status_code == 200
    assert response.content == 'ok'
    assert requests_post.call_count == 1

    assert requests_post.call_args == mock.call(URL, data=mock.ANY, headers={'content-type': 'application/json'})
    payload = json.loads(requests_post.call_args[1]['data'])
    assert isinstance(payload, dict)
    assert set(payload.keys()) == set(['data', 'caller', 'callee', 'event'])
    assert payload['event'] == 'stop'
    assert payload['data'] == {'user': 'zozo'}
    assert payload['callee'] == '102'
    assert payload['caller'] == '0033699999999'

@mock.patch('requests.post')
def test_call_start_with_id(requests_post, client):
    URL = 'http://welco.example.net/'
    TlmCom.objects.create(slug='tlmcom', description='tlmcom', welco_url=URL)
    url = reverse('tlmcom-call-start', kwargs={'slug': 'tlmcom'}) \
        + '?caller=0033699999999&callee=102&id=.zozo'
    response = client.get(url)
    assert response.status_code == 200
    assert response.content == 'ok'
    assert requests_post.call_count == 1

    assert requests_post.call_args == mock.call(URL, data=mock.ANY, headers={'content-type': 'application/json'})
    payload = json.loads(requests_post.call_args[1]['data'])
    assert isinstance(payload, dict)
    assert set(payload.keys()) == set(['caller', 'callee', 'event'])
    assert payload['event'] == 'start'
    assert payload['callee'] == 'zozo'
    assert payload['caller'] == '0033699999999'

@mock.patch('requests.post')
def test_call_stop_with_id(requests_post, client):
    URL = 'http://welco.example.net/'
    TlmCom.objects.create(slug='tlmcom', description='tlmcom', welco_url=URL)
    url = reverse('tlmcom-call-stop', kwargs={'slug': 'tlmcom'}) \
        + '?caller=0033699999999&callee=102&id=.zozo'
    response = client.get(url)
    assert response.status_code == 200
    assert response.content == 'ok'
    assert requests_post.call_count == 1

    assert requests_post.call_args == mock.call(URL, data=mock.ANY, headers={'content-type': 'application/json'})
    payload = json.loads(requests_post.call_args[1]['data'])
    assert isinstance(payload, dict)
    assert set(payload.keys()) == set(['caller', 'callee', 'event'])
    assert payload['event'] == 'stop'
    assert payload['callee'] == 'zozo'
    assert payload['caller'] == '0033699999999'
