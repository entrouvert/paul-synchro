import pytest
from httmock import urlmatch, HTTMock, response

import django_webtest


@pytest.fixture
def app(request):
    wtm = django_webtest.WebTestMixin()
    wtm._patch_settings()
    request.addfinalizer(wtm._unpatch_settings)
    return django_webtest.DjangoTestApp()


@urlmatch(netloc='^api-adresse.data.gouv.fr$', path='^/search/$')
def api_adresse_data_gouv_fr_search(url, request):
    return response(200, {
        "limit": 1,
        "attribution": "BAN",
        "version": "draft",
        "licence": "ODbL 1.0",
        "query": "plop",
        "type": "FeatureCollection",
        "features": [
            {
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        -0.593775,
                        47.474633
                    ]
                },
                "properties": {
                    "citycode": "49007",
                    "name": "Rue Roger Halope",
                    "id": "49007_6950_be54bd",
                    "city": "Angers",
                    "context": "49, Maine-et-Loire, Pays de la Loire",
                    "score": 0.14097272727272728,
                    "label": "Rue Roger Halope 49000 Angers",
                    "postcode": "49000",
                    "type": "street"
                },
                "type": "Feature"
            }
        ]}, request=request)


@urlmatch(netloc='^api-adresse.data.gouv.fr$', path='^/reverse/$')
def api_adresse_data_gouv_fr_reverse(url, request):
    return response(200, {
        "limit": 1,
        "attribution": "BAN",
        "version": "draft",
        "licence": "ODbL 1.0",
        "type": "FeatureCollection",
        "features": [
            {
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        -0.593775,
                        47.474633
                    ]
                },
                "properties": {
                    "citycode": "49007",
                    "name": "Rue Roger Halope",
                    "id": "49007_6950_be54bd",
                    "city": "Angers",
                    "distance": 0,
                    "context": "49, Maine-et-Loire, Pays de la Loire",
                    "score": 1.0,
                    "label": "Rue Roger Halope 49000 Angers",
                    "postcode": "49000",
                    "type": "street"
                },
                "type": "Feature"
            }
        ]}, request=request)


@pytest.yield_fixture
def mock_api_adresse_data_gouv_fr_search():
    with HTTMock(api_adresse_data_gouv_fr_search):
        yield None


@pytest.yield_fixture
def mock_api_adresse_data_gouv_fr_reverse():
    with HTTMock(api_adresse_data_gouv_fr_reverse):
        yield None
