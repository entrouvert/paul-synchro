LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'

KNOWN_SERVICES = {
    'wcs': {
        'wcs1': {
            'url': 'http://example.org/',
            'verif_orig': 'wcs1',
            'secret': 'abcde',
        }
    }
}

# include all contrib apps
INSTALLED_APPS += (
        'passerelle.contrib.agoraplus',
        'passerelle.contrib.arcgis',
        'passerelle.contrib.fake_family',
        'passerelle.contrib.greco',
        'passerelle.contrib.iparapheur',
        'passerelle.contrib.maarch',
        'passerelle.contrib.mdel',
        'passerelle.contrib.meyzieu_newsletters',
        'passerelle.contrib.nancypoll',
        'passerelle.contrib.seisin_by_email',
        'passerelle.contrib.solis_apa',
        'passerelle.contrib.stub_invoices',
        'passerelle.contrib.teamnet_axel',
        'passerelle.contrib.tlmcom',
        )

PASSERELLE_APP_FAKE_FAMILY_ENABLED = True
PASSERELLE_APP_TLMCOM_ENABLED = True
PASSERELLE_APP_IPARAPHEUR_ENABLED = True
PASSERELLE_APP_MEYZIEU_NEWSLETTERS_ENABLED = True
PASSERELLE_APP_NANCYPOLL_ENABLED = True
PASSERELLE_APP_ARCGIS_ENABLED = True
PASSERELLE_APP_SOLIS_APA_ENABLED = True
PASSERELLE_APP_MDEL_ENABLED = True
