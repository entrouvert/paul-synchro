import json
from StringIO import StringIO

import pytest

from django.core.files import File
from django.core.urlresolvers import reverse
from django.contrib.contenttypes.models import ContentType
from django.test import Client

from passerelle.base.models import ApiUser, AccessRight

from passerelle.contrib.nancypoll.models import NancyPoll

data = """
0,999,,,S,A,Z,,34,28,ECOLE MATERNELLE DIDION,16 RUE SAINT THIEBAUT,,JOFFRE,BOULEVARD JOFFRE,4,NANCY 1,0,1083,1
0,121,,,I,A,Z,,46,3,GYMNASE MARTINY,10 RUE VICTOR,,LOBAU,BOULEVARD LOBAU,2,NANCY  3,0,1083,1
0,999,,,I,A,Z,,28,37,ECOLE MATERNELLE CHARLEMAGNE,RUE DU CHANOINE BLAISE,,BLANC,RUE DU CHEMIN BLANC,4,NANCY 1,0,1083,1
0,999,,,P,A,Z,,22,39,ECOLE MATERNELLE DE BUTHEGNEMONT,45 RUE ANNE FERIET,,BLANC,RUE DU CHEMIN BLANC,4,NANCY 1,0,1083,1
0,9999,,,S,A,Z,,3,43,HOTEL DE VILLE,1 PLACE STANISLAS,,BLANC,RUE DU CHEVAL BLANC,3,NANCY 2,0,1083,1
0,9,,,I,A,Z,,25,32,ECOLE MATERNELLE STANISLAS,4 TER RUE VICTOR HUGO,,HUGO,RUE VICTOR HUGO,4,NANCY 1,0,1083,1
12,9999,,,P,A,Z,,24,31,ECOLE MATERNELLE STANISLAS,4 TER RUE VICTOR HUGO,,HUGO,RUE VICTOR HUGO,4,NANCY 1,0,1083,1
0,15,,,I,A,Z,,32,64,ECOLE ELEMENTAIRE CHARLEMAGNE,RUE LAVIGERIE,,LEMOINE,RUE VICTOR LEMOINE,4,NANCY 1,0,1083,1
0,20,,,P,A,Z,,32,64,ECOLE ELEMENTAIRE CHARLEMAGNE,RUE LAVIGERIE,,LEMOINE,RUE VICTOR LEMOINE,4,NANCY 1,0,1083,1
15,9999,,,I,A,Z,,37,13,ECOLE ELEMENTAIRE MARCEL LEROY,22 RUE DE GRAFFIGNY,,LEMOINE,RUE VICTOR LEMOINE,2,NANCY  3,0,1083,1
20,9999,,,P,A,Z,,37,13,ECOLE ELEMENTAIRE MARCEL LEROY,22 RUE DE GRAFFIGNY,,LEMOINE,RUE VICTOR LEMOINE,2,NANCY  3,0,1083,1
0,21,,,I,A,Z,,51,19,GYMNASE BOURGUIGNON,5 RUE DE TURINAZ,,JUIN,RUE DU MARECHAL JUIN,2,NANCY  3,0,1083,1
0,20,,,P,A,Z,,51,19,GYMNASE BOURGUIGNON,5 RUE DE TURINAZ,,JUIN,RUE DU MARECHAL JUIN,2,NANCY  3,0,1083,1
20,52,,,P,A,Z,,50,18,GYMNASE BOURGUIGNON,5 RUE DE TURINAZ,,JUIN,RUE DU MARECHAL JUIN,2,NANCY  3,0,1083,1
21,39,,,I,A,Z,,50,18,GYMNASE BOURGUIGNON,5 RUE DE TURINAZ,,JUIN,RUE DU MARECHAL JUIN,2,NANCY  3,0,1083,1
39,9999,,,I,A,Z,,49,20,GYMNASE BOURGUIGNON,5 RUE DE TURINAZ,,JUIN,RUE DU MARECHAL JUIN,2,NANCY  3,0,1083,1
"""


def parse_response(response):
    return json.loads(response.content)['data']


@pytest.fixture
def setup(db):
    api = ApiUser.objects.create(username='all',
                                 keytype='', key='')
    poll = NancyPoll.objects.create(csv_file=File(StringIO(data), 'data.csv'),
                                    slug='poll')
    obj_type = ContentType.objects.get_for_model(poll)
    AccessRight.objects.create(codename='can_access', apiuser=api,
                               resource_type=obj_type, resource_pk=poll.pk)
    url = reverse('generic-endpoint', kwargs={
                  'connector': 'nancypoll', 'slug': poll.slug, 'endpoint': 'data'})
    return url, Client()


def test_failure(setup):
    url, client = setup
    qs = {'street_no': '37000', 'street_name': 'Rue du Marechal Juin'}
    resp = json.loads(client.get(url, qs).content)
    assert resp['err_desc'] == 'Polling Station Not Found'
    assert int(resp['err']) != 0


def test_no_params(setup):
    url, client = setup
    qs = {}
    resp = client.get(url, qs)
    assert resp.status_code == 200
    assert json.loads(resp.content)['err_desc'] == 'All parameters are required'
    assert int(json.loads(resp.content)['err']) != 0


def test_invalid_street_no(setup):
    url, client = setup
    qs = {'street_no': 'lol', 'street_name': 'whatever'}
    resp = client.get(url, qs)
    assert resp.status_code == 200
    assert json.loads(resp.content)['err_desc'] == 'Invalid street no value'
    assert int(json.loads(resp.content)['err']) != 0


def test_success_i_side(setup):
    url, client = setup
    qs = {'street_no': '37', 'street_name': 'Rue du Marechal Juin'}
    result = parse_response(client.get(url, qs))
    assert result['id'] == '18'
    assert result['code'] == '50'
    assert result['text'] == 'GYMNASE BOURGUIGNON'
    assert result['address'] == '5 RUE DE TURINAZ'
    assert result['canton'] == 'NANCY  3'


def test_success_p_side(setup):
    url, client = setup
    qs = {'street_no': '18', 'street_name': 'Rue du Chemin Blanc'}
    result = parse_response(client.get(url, qs))
    assert result['id'] == '39'
    assert result['code'] == '22'
    assert result['text'] == 'ECOLE MATERNELLE DE BUTHEGNEMONT'
    assert result['address'] == '45 RUE ANNE FERIET'
    assert result['canton'] == 'NANCY 1'
