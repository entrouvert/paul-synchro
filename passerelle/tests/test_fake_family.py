# -*- coding: utf-8 -*-

import json

from django.core.wsgi import get_wsgi_application
from django.core.urlresolvers import reverse

import pytest
from passerelle.contrib.fake_family.models import FakeFamily

from webtest import TestApp

pytestmark = pytest.mark.django_db

def test_init_fake_family():
    fake = FakeFamily.objects.create()
    jsondb = fake.jsondatabase
    assert len(jsondb['adults']) == 22
    assert len(jsondb['children']) == 50
    family_id = fake.get_familyid_by_nameid('fake_nameid')
    assert family_id == '1'
    family = jsondb['families'][family_id]
    assert len(fake.get_list_of('adults', family_id)) == len(family['adults'])
    assert len(fake.get_list_of('children', family_id)) == len(family['children'])


@pytest.fixture
def setup():
    app = TestApp(get_wsgi_application())
    fakefam = FakeFamily.objects.create(title='fakefam', slug='fakefam')
    return app, fakefam

def test_fake_family_dump(setup):
    app, fakefam = setup
    resp = app.get(reverse('fake-family-dump', kwargs={'slug': fakefam.slug}))
    assert resp.json == fakefam.jsondatabase

def test_fake_family_urls(setup):
    app, fakefam = setup
    name_id = '__test_name_id__'
    family = fakefam.jsondatabase['families']['1']
    adult_id = family['adults'][0]
    adult = fakefam.jsondatabase['adults'][adult_id]
    login = adult['login']
    password = adult['password']
    resp = app.get(reverse('fake-family-link', kwargs={'slug': fakefam.slug}) + \
            '?NameID=%s&login=%s&password=%s' % (name_id, login, password))
    assert resp.json['err'] == 0
    assert resp.json['data'] == adult
    resp = app.get(reverse('fake-family-info', kwargs={'slug': fakefam.slug}) + \
            '?NameID=%s' % name_id)
    assert resp.json['err'] == 0
    assert resp.json['data']['adults'][0]['address'] == adult['address']
    resp = app.get(reverse('fake-family-key', kwargs={'slug': fakefam.slug, 'key': 'adults'}) + \
            '?NameID=%s' % name_id)
    assert resp.json['err'] == 0
    assert resp.json['data'][0]['address'] == adult['address']
    resp = app.get(reverse('fake-family-key', kwargs={'slug': fakefam.slug, 'key': 'children'}) + \
            '?NameID=%s' % name_id)
    assert resp.json['err'] == 0
    assert len(resp.json['data']) == 2
    resp = app.get(reverse('fake-family-unlink', kwargs={'slug': fakefam.slug}) + \
            '?NameID=%s' % name_id)
    assert resp.json['err'] == 0
    assert resp.json['data'] == 'ok'
    resp = app.get(reverse('fake-family-unlink', kwargs={'slug': fakefam.slug}) + \
            '?NameID=%s' % name_id)
    assert resp.json['err'] == 0
    assert resp.json['data'] == 'ok (but there was no links)'

def test_fake_family_bad_login(setup):
    app, fakefam = setup
    name_id = '__test_name_id__'
    family = fakefam.jsondatabase['families']['1']
    adult_id = family['adults'][0]
    adult = fakefam.jsondatabase['adults'][adult_id]
    login = adult['login']
    password = 'bad' + adult['password']
    resp = app.get(reverse('fake-family-link', kwargs={'slug': fakefam.slug}) + \
            '?NameID=%s&login=%s&password=%s' % (name_id, login, password),
            status=403)
    assert resp.json['err'] == 100
    assert resp.json['err_desc'] == 'bad password'
    login = 'bad@login'
    resp = app.get(reverse('fake-family-link', kwargs={'slug': fakefam.slug}) + \
            '?NameID=%s&login=%s&password=%s' % (name_id, login, password),
            status=403)
    assert resp.json['err'] == 100
    assert resp.json['err_desc'] == 'unknown login'
