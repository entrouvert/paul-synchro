from django.conf.urls import include, url
from django.contrib import admin

from . import views
from saml.decorators import user_can_declare, user_not_in_ldap
from saml import views as samlviews
from django.contrib.auth.decorators import login_required

project_name = 'sp_sso'
urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^register/', include('saml.urls')),
    url(r'^blank/', user_not_in_ldap(samlviews.RegistrationFormView.as_view()), name="blankregister"),
    url(r'^invite/', include('invite.urls'), name="invite"),
    url(r'^declare/$', login_required(user_can_declare(views.declare)), name="declare"),
    url(r'^declare/subscribed/$', views.subscribed, name='subscribed'),
    url(r'^$', views.index),
    url(r'^accounts/mellon/', include('mellon.urls'), {'project_name': project_name}),
    url(r'^logout/$', views.logout, name='auth_logout'),
    url(r'^login/$', views.login, name='auth_login'),
    url(r'^logged_in/', views.logged_in, name='auth_logged_in'),
]
