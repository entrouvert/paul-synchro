"""
Django settings for sp_sso project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

from django.conf import global_settings
from django.utils.translation import ugettext_lazy as _

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Logger config copied from the authentic2 project settings:
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            },
    },
    'loggers': {
        'django.request': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'DEBUG'),
            'propagate': True,
        },
        'sp_sso.resource': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'DEBUG'),
            'propagate': True,
        },
    },
}

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '3568aw3i!gv@xb_nloh0$(lz%^y-2-&ey_qx^i)&_j!26#q)73'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['localhost', 'sp-condorcet.dev.entrouvert.org']

TEMPLATE_DIRS = (
    'templates',
)

USE_I18N = False

AUTHENTICATION_BACKENDS = (
    'mellon.backends.SAMLBackend',
    'django.contrib.auth.backends.ModelBackend',
)

AUTH_USER_MODEL = 'saml.SupAnnUser'

LOGIN_REDIRECT_URL = '/logged_in/'
LOGIN_URL = '/login/'
LOGOUT_URL = '/logout/'

MELLON_ATTRIBUTE_MAPPING = {
    'prenom': 'urn:oid:2.5.4.42', # givenName
    'nom': 'urn:oid:2.5.4.4', # sn
    'email' : 'urn:oid:0.9.2342.19200300.100.1.3', # mail
    'ep_principal_name' : 'urn:oid:1.3.6.1.4.1.5923.1.1.1.10', # workaround for p13
    'ep_principal_name' : 'urn:oid:1.3.6.1.4.1.5923.1.1.1.6', # eduPersonPrincipalName
    #'ep_principal_name' : 'eppn', # eduPersonPrincipalName at Paris13
    's_etablissement' : 'urn:oid:1.3.6.1.4.1.7135.1.2.1.14', # supannEtablissement
    'ep_primary_affiliation' : 'urn:oid:1.3.6.1.4.1.5923.1.1.1.5', # eduPersonPrimaryAffiliation
    'ep_affiliation' : 'urn:oid:1.3.6.1.4.1.5923.1.1.1.1', # eduPersonPrimaryAffiliation
    's_entite_affectation_principale' : 'urn:oid:1.3.6.1.4.1.7135.1.2.1.13', # supannEntiteAffectationPrincipale
    's_entite_affectation' : 'urn:oid:1.3.6.1.4.1.7135.1.2.1.8', # supannEntiteAffectation
    's_emp_corps' : 'supannEmpCorps', # supannEmpCorps
    's_liste_rouge' : 'urn:oid:1.3.6.1.4.1.7135.1.2.1.1', # supannListeRouge
}

MELLON_SUPERUSER_MAPPING = {
    'is_superuser': 'true',
}

# The official Condorcet IdP uses a transient NameID attribute. It also
# forwards on a eduTargetedID.
#MELLON_TRANSIENT_FEDERATION_ATTRIBUTE = 'urn:oid:1.3.6.1.4.1.5923.1.1.1.10' # eduTargetedID
MELLON_TRANSIENT_FEDERATION_ATTRIBUTE = 'urn:oid:1.3.6.1.4.1.5923.1.1.1.6' # EPPN

MELLON_IDENTITY_PROVIDERS = [
    {'METADATA_URL': 'http://idp-condorcet.dev.entrouvert.org/idp/saml2/metadata'}, # PoC IdP
    {'METADATA_URL': 'https://idp-campus-condorcet.renater.fr/idp/shibboleth'}, # CC IdP
    {'METADATA_URL': 'https://shibboleth.ehess.fr/idp/shibboleth'}, # EHESS IdP
    {'METADATA_URL': 'https://federation-identite.univ-paris13.fr/idp/shibboleth'}, # Paris 13 IdP
    {'METADATA_URL': 'https://shibbov3.ephe.fr/idp/shibboleth'}, # EPHE IdP
    {'METADATA_URL': 'https://idp-test.univ-paris1.fr/idp/shibboleth'} # Paris 1 Test IdP
]

MELLON_DISCOVERY_SERVICE_URL = "https://discovery.renater.fr/test"

MELLON_PUBLIC_KEYS = ('''MIIC+TCCAeGgAwIBAgIJAPDzLp0rbCqRMA0GCSqGSIb3DQEBCwUAMBMxETAPBgNV
BAMMCHdob2NhcmVzMB4XDTE3MDMwODE2MjYyNloXDTI3MDMwNjE2MjYyNlowEzER
MA8GA1UEAwwId2hvY2FyZXMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIB
AQDEgvuUDz+6Gsn/DGwWn2rfkGdCBXIvJzCD7zpHIX2rZGZMcbo6SoDW+NG/N+D0
27j4/A+9LgXaIDMZaUx0ICtu5vzqQMLjKhmhq8HyA9yfvGtexxETyli5YkM0B4Q8
uO2NMlGX9zQKjox7ebjJhF0PCPmRaq9ZGJSJFS8ywU3prqm2RORUkwBeGL7oJPoz
WTb/1kU4qPXBXeiMaL/3xzVi9VrJSNs69oOIKvnfLX+i8hZZL8qPtfMhycv9E12M
BI/qKxS3fqCcVb+GKNl1DwivSz2bkzu1a5azrm9JmjxAxh1cv5yWJjotmFkjCbad
Q/gSgdcN22Fi53jv3LXpw+PZAgMBAAGjUDBOMB0GA1UdDgQWBBQyz+pQ7a1q0viR
oGrFnu2VJzzy2DAfBgNVHSMEGDAWgBQyz+pQ7a1q0viRoGrFnu2VJzzy2DAMBgNV
HRMEBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQAJi1EulQuYj6rhGvjd7/0pOcR/
gWQhvYVTSbKG44WnPv2cpkVnHUvrqJ7i+THNwP+Txm+12NDBJ6AtmLmT3ATWdBGU
ireT6HVAxl/bE2RcbKi5D3r2VM4DT1xQQhU1pdwwkbMuHrt5pFT4TfBu561qQFFx
2P8D+Y0skOOygVfhPZPTic14ofrygFC3nBnv/lvQQLDRSK25xDX3X34P8f+kXsBq
rTxyxxM4b3mhNbgaXnRzO5C962wEcw1RwhnCm5jUqHXERn6kNM63jGmh2owjeiWa
F31DJoxr0oiq9meJQuqJbFEyCY7TtAc41uIKf4tJr6kLIsbSOy6WLfdtQ6sB''', )

MELLON_PRIVATE_KEY = '''MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDEgvuUDz+6Gsn/
DGwWn2rfkGdCBXIvJzCD7zpHIX2rZGZMcbo6SoDW+NG/N+D027j4/A+9LgXaIDMZ
aUx0ICtu5vzqQMLjKhmhq8HyA9yfvGtexxETyli5YkM0B4Q8uO2NMlGX9zQKjox7
ebjJhF0PCPmRaq9ZGJSJFS8ywU3prqm2RORUkwBeGL7oJPozWTb/1kU4qPXBXeiM
aL/3xzVi9VrJSNs69oOIKvnfLX+i8hZZL8qPtfMhycv9E12MBI/qKxS3fqCcVb+G
KNl1DwivSz2bkzu1a5azrm9JmjxAxh1cv5yWJjotmFkjCbadQ/gSgdcN22Fi53jv
3LXpw+PZAgMBAAECggEAD03kKfPGEshjUum0wU5JFIA6innCGsoCjUPkVgXVs7Nu
BCYXStLildtq+mlvq6IxFbMLxr/+1K4NTL1WpvSabViv+c5xXx+4P08RG7gRRp91
/TujqNbpcgalFThApDoCxixVDnDCLNBNQJCKQ6d7V+BrD1gvr9CqQMLVmi5T0w8U
UP6EXz04Tycv/9KoPTiuFMtv9ei2Eo6Egt807MDa1W8I2dhack17nq7tiIsJgIiB
rEr3fRXgHDfvIoy9AjccqgpcE5+KFVTYW5BumbAyHXuJWEM1JHSdICoVkQasZwMJ
VY1yhj9MnYlHZRTwuB0LvgU5Eqf0ZLFKzoNn1DjMwQKBgQDp7MZrfW29qHb7Pc6m
O5NjQe6VlMx/0a//2O2NrBiSqALNyPpAqTz9eML9w35kUqnbTEkikCZsV9PG/Uxg
WDZ4JHJE3M/gn/dOGb2wQMkzVlqKEJL70T3/Y98ucjJVczdEKOivRq6cQXb+g1uG
NxVos1ts0meMNFVPVm+w3SNLTwKBgQDXDl4aQcybKtLCguGPXApGoImN3gJhTvKR
9eck2wqLJGIvdwSOzLZq95q7iGUsY3EMQ6PbweZWiI3tM1hD4CM3AymooeyoeAJF
Q9ozIRKu8YjV3DopCWgYRp7QvE8nHqOyQFbsUpX39kRqwZHPcYdx7SVBti9EGJ45
qMNfWaL0VwKBgQDOq1ceboF/N5BvJkslyKrN3dlZJE6MewjAxLoUGcDYCUH456BZ
cmwlEQVNR+Op5PKWgk53D94yiGHdg7pvwF/XQ3QFbQTn0syentVdG8/m4Fs7cas6
ecIJmgX7Nf9MSW/Bc1coTA1Glfv4nrtE1/O9PgD/IuFDGJVqc9ZX7vgYtwKBgQC8
PyhiRYyrmr2oZFa7Xa7lCeu+kkJ1GV0+JQky+sXs7GlB8SW9NjbLxXfdhlRBmb5A
JQlO+Rj+UEQkCRjPOWi54/vYT/1PrtUV+oIK1X18Q5Mq2L7UYxaKgW7NeriynqBe
DPHJpbT+u8RByrUu58rPD+3X6njPW9lM0YxLb7QzuwKBgHMJZvc5YUJ6/TXrsoVg
MnwMlHDoIsUWFdWO4O5WaHszgA4ST0S2NWvLq+7dVREPMCpIjoZrGcXZlVxTiTbE
wMXFR0bCFblwVciyltVp5YyvMy+yBPW+8xAQ4hCHXeWGbV2iNYEsEHoiGSBfVmCM
JT6ZsP4RglqmN7XNUebJ/ogN'''

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'saml',
    'invite',
    'mellon',
    'gadjo',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
)

ROOT_URLCONF = 'sp_sso.urls'

WSGI_APPLICATION = 'sp_sso.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

SESSION_ENGINE = 'django.contrib.sessions.backends.signed_cookies'

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = False

USE_TZ = True

LANGUAGES = (
    ('fr', _('French')),
    ('en', _('English')),
)

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale_dir/'),
)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = 'collected_static'

STATICFILES_DIRS = (os.path.join(BASE_DIR, 'static'),)
STATICFILES_FINDERS = global_settings.STATICFILES_FINDERS + ('gadjo.finders.XStaticFinder',)

SESSION_EXPIRE_AT_BROWSER_CLOSE = False
SESSION_COOKIE_AGE = 30 * 60
