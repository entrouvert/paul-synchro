import logging
from django.views.generic import FormView
from django.utils.translation import ugettext_lazy as _

from .forms import InvitationForm
from .utils import do_invite, get_invitaton_attributes_mapping, \
        get_affectations_from_eppn

from saml.utils import render_message


logger = logging.getLogger('django')

MSG_INVITATION_SENT = _("Your invitation has been sent.")


def invitation_sent(request):
    """
    Notifies the user about her invitation(s) by displaying a validation
    message.
    """
    return render_message(request, MSG_INVITATION_SENT)


class InvitationFormView(FormView):
    """
    Main FormView for the invitation process.

    Gathers SSO attributes from the identity provider, used to pre-fill and
    set as readonly the `InvitationForm` fields.
    """
    form_class = InvitationForm
    template_name = 'invite/invitation_form.html'
    success_url = '/invite/sent'
    allowed_email_delimiters=[';', ',', ' ']

    def get_initial(self):
        initial = super(InvitationFormView, self).get_initial()

        # User is logged in the identity provider
        if 'mellon_session' in self.request.session:

            # Fetch SSO attributes
            for attribute_key, attribute_value in \
                    get_invitaton_attributes_mapping().items():
                if hasattr(self.request.user, attribute_key):
                    attribute_element = getattr(self.request.user, attribute_key, '')
                    initial[attribute_value] = attribute_element

            initial['hote_commentaire'] = '''EduPersonPrincipalName de 
                    l\'invitant'''

        initial['hote_type'] = self.request.session.get('host_type')
        return initial

    def form_valid(self, form):
        invitation = {}
        post_dict = self.request.POST
        logger.info("Invitation POST data : {}".format(''))

        # Craft any relevant invitation data
        for form_attribute in self.form_class.base_fields.keys():
            sanitized_form_entry = post_dict.get(form_attribute, False)
            if sanitized_form_entry:
                invitation[form_attribute] = sanitized_form_entry

        invitation['type'] = post_dict.get('hote_type')
        multiple_email_invitation = {}

        # Send as many invites as there are email adresses (blank-separated)
        for delimiter in self.allowed_email_delimiters:
            invitation['email'] = invitation['email'].replace(delimiter, ' ')
        for email in invitation['email'].split():
            multiple_email_invitation = invitation.copy()
            multiple_email_invitation['email'] = email
            do_invite(multiple_email_invitation)

        return super(InvitationFormView, self).form_valid(form)
