from django.conf.urls import url

from . import views
from saml.decorators import user_in_ldap
from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^$', login_required(user_in_ldap(views.InvitationFormView.as_view())), name='invitation'),
    url(r'^sent/$', views.invitation_sent , name='sent'),
]
