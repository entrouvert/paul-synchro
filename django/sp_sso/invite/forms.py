from django.utils.translation import ugettext_lazy as _
from django import forms

from .utils import get_invitaton_attributes_mapping, get_additional_prefilled_fields, \
        ADDITIONAL_PREFILLED_FIELDS
from saml.utils import ldap_get_description_etablissements, \
        ldap_get_description_unites



class InvitationForm(forms.Form):
    """ Used when sending invites to the Campus

    Invites are sent to the `email` field. Multiple adresses are handled if
    they are separated by a blank space ` ` character.

    This form is restricted to users having previously registered to the
    Campus ; therefore they must log in to their RENATER-federated account
    before accessing the form.
    """
    def __init__(self, *args, **kwargs):
        """
        Attributes fetched from the identity provider are used to pre-fill
        the fields.
        An extra hidden field is used to store the user's
        eduPersonPrincipalName, used as key identity attribute in the
        corresponding LDAP entry.
        """
        super(InvitationForm, self).__init__(*args, **kwargs)
        # Add a help text for sending multiple invitations
        self.fields['email'].help_text = _(
            _('Blank-separated emails if multiple adresses.'))
        # Pre-filled fields are readonly
        for prefilled_field in get_invitaton_attributes_mapping().values() + \
                get_additional_prefilled_fields():
            if  kwargs['initial'].get(prefilled_field):
                self.fields[prefilled_field].widget.attrs['readonly'] = True

        etablissement_code = kwargs['initial'].get('hote_etablissement')
        self.fields['hote_etablissement_description'].initial = \
                ldap_get_description_etablissements(etablissement_code)
        if not self.fields['hote_etablissement_description'].initial :
            self.fields['hote_etablissement_description'].initial = etablissement_code
        else:
            self.fields['hote_etablissement_description'].widget.attrs['readonly'] = True

        unite_code = kwargs['initial'].get('hote_unite')
        self.fields['hote_unite_description'].initial = ldap_get_description_unites(unite_code)
        if not self.fields['hote_unite_description'].initial:
            self.fields['hote_unite_description'].initial = unite_code
        else:
            self.fields['hote_unite_description'].widget.attrs['readonly'] = True

    # Blank-seperated list of recipient email adresses
    email = forms.CharField(
            max_length=100,
            label=_('Email address(es) where to send an invite'))
    # Invitation message that will be sent with the invite
    message = forms.CharField(
            widget=forms.Textarea(), max_length=999,
            label=_("Invitation message"), required=False)

    # Host identity
    #  If the RENATER identity federation is used, this field will be pre-
    #  filled with the host's EPPN (EduPersonPrincalName)
    hote_identite = forms.CharField(
            max_length=100, label=_("Your RENATER identifier"))

    hote_nom = forms.CharField(
            max_length=100, label=_("Your lastname"))

    hote_prenom = forms.CharField(
            max_length=100, label=_("Your firstname"))

    hote_etablissement = forms.CharField(
            max_length=100, label=_("Your institution"), required=False)

    hote_unite = forms.CharField(
            max_length=100, label=_("Your research unit"), required=False)

    hote_etablissement_description = forms.CharField(
            max_length=100, label=_("Your institution"), required=False)

    hote_unite_description = forms.CharField(
            max_length=100, label=_("Your research unit"), required=False)

    hote_courriel = forms.CharField(
            max_length=100, label=_("Your email address"))

    hote_type = forms.CharField(
            max_length=100, label="hidden host type", required=False)
