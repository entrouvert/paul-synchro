from django.db import models
from django.contrib.auth.models import AbstractUser


class SupAnnUser(AbstractUser):
    """ Base user of the SSO scheme. OBSOLETE

    eduPerson and supann2009 attributes have been added so that theses
    attributes can be retrieved in mellon.

    XXX mellon attributes are also stored in the session.
    (see the request.session['mellon_session'] dict)
    """
    prenom = models.CharField(max_length=100,default='user_eppn')
    nom = models.CharField(max_length=100,default='user_eppn')
    # eduPerson attributes:
    ep_principal_name = models.CharField(max_length=100,default='user_eppn')
    ep_primary_affiliation = models.CharField(max_length=100, default="")
    ep_affiliation = models.CharField(max_length=100, default="")
    # supannPerson attributes:
    s_entite_affectation_principale = models.CharField(max_length=100, default="")
    s_entite_affectation = models.CharField(max_length=100, default="")
    s_emp_corps = models.CharField(max_length=100, default="")
    s_etablissement = models.CharField(max_length=100, default="")
    #invite_unite = models.CharField(max_length=100, default="")
    s_liste_rouge = models.BooleanField(default=False)

    REQUIRED_FIELDS = []
