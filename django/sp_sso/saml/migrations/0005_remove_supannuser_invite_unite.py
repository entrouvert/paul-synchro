# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('saml', '0004_supannuser_invite_unite'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='supannuser',
            name='invite_unite',
        ),
    ]
