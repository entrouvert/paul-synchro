# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('saml', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='supannuser',
            name='ep_principal_name',
            field=models.CharField(default=b'user_eppn', max_length=100),
        ),
    ]
