# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('saml', '0003_delete_unit'),
    ]

    operations = [
        migrations.AddField(
            model_name='supannuser',
            name='invite_unite',
            field=models.CharField(default=b'', max_length=100),
        ),
    ]
