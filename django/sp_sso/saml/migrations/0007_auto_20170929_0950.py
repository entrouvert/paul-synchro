# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('saml', '0006_auto_20170929_0933'),
    ]

    operations = [
        migrations.RenameField(
            model_name='supannuser',
            old_name='s_etablissement_raw',
            new_name='s_etablissement',
        ),
    ]
