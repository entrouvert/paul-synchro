# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('saml', '0002_auto_20170503_0925'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Unit',
        ),
    ]
