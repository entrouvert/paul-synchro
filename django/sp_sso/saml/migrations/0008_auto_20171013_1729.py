# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('saml', '0007_auto_20170929_0950'),
    ]

    operations = [
        migrations.AddField(
            model_name='supannuser',
            name='nom',
            field=models.CharField(default=b'user_eppn', max_length=100),
        ),
        migrations.AddField(
            model_name='supannuser',
            name='prenom',
            field=models.CharField(default=b'user_eppn', max_length=100),
        ),
    ]
