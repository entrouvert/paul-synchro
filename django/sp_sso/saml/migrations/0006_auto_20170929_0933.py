# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('saml', '0005_remove_supannuser_invite_unite'),
    ]

    operations = [
        migrations.RenameField(
            model_name='supannuser',
            old_name='s_etablissement',
            new_name='s_etablissement_raw',
        ),
    ]
