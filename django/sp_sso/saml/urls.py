from django.conf.urls import url

from . import views
from .decorators import user_not_in_ldap
from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^$', login_required(user_not_in_ldap(views.RegistrationFormView.as_view())), name='register'),
    #url(r'blank^$', user_not_in_ldap(views.RegistrationFormView.as_view()), name='blankregister'),
    url(r'^wcs_post/$', views.wcs_post , name='wcs_post'),
]
