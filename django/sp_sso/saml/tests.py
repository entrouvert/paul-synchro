from django.test import TestCase
from .utils import ldap_init, wcs

import requests

# Create your tests here.
class ServicesTestCase(TestCase):
    """Ensure that all four services are up and running."""
    def test_ldap_connection(self):
        l = ldap_init()
        self.assertTrue(l)

    def test_wcs_connection(self):
        response = requests.get(wcs)
        self.assertEqual(response.status_code, 200)

    def test_passerelle_connection(self):
        response = requests.get('http://dir-condorcet.dev.entrouvert.org')
        self.assertEqual(response.status_code, 200)

    def test_authentic_connection(self):
        response = requests.get('http://idp-condorcet.dev.entrouvert.org')
        self.assertEqual(response.status_code, 200)
