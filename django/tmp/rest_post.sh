#!/bin/sh

curl -H "Content-type: application/json" \
     -H "Accept: application/json" \
     -d@sample.json \
     http://wcs.example.com/api/formdefs/identity/submit
