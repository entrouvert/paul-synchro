#!/bin/sh

curl -H "Content-type: application/json" \
     -H "Accept: application/json" \
     -d@new_form.json \
     http://wcs.example.com/api/formdefs/
