# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2016 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms
from .models import Query


class QueryForm(forms.ModelForm):
    class Meta:
        model = Query
        widgets = {
            'resource': forms.HiddenInput(),
            'filters': forms.Textarea(attrs={'rows': 2}),
            'attributes' : forms.Textarea(attrs={'rows': 2}),
            'scope' : forms.RadioSelect,
            'distinguished_name' : forms.Textarea(attrs={'rows': 2}),
            'password' : forms.PasswordInput,
        }
        fields = '__all__'
