Entr'ouvert

Projet de fin d'études : Rapport de stage

Université de Technologie de Compiègne

Paul Marillonnet

# Introduction

## Remerciements

## Enjeux du stage

# Présentation d’Entr’ouvert

## Présentation de la structure de SCOP

## La distribution de logiciel libre

### La licence libre

## Le travail avec les collectivités

## La gestion des projets EO

### Git/gitolite

### Redmine

### Bistro

# Cas d’usage

## Approche fonctionnelle du problème
 
## Synthèse des différentes technologies disponibles

//Une seule page maxi

# État de l’art de la gestion des identités numériques

## Les solutions libres pour la synchronisation et l’approvisionnement des référentiels d’identités numériques

# Prise en main des technologies

## Django

## w.c.s / Passerelle

## ReSTful APIs

# Mise en œuvre et réalisations

## Démonstrateur avec LSC (*LDAP Synchronization Connector*)

## Développement pour le Campus Condorcet : gestion des comptes invités

## Analyse des besoins pour les outils Entr’ouvert

### Architecture cible

### Développements nécessaires

# Synthèse technique

// deux pages maxi

# Conclusion(s)

# Annexes

## Acronymes

## Bibliographie

//titre source date auteur

## Extraits de code source

//Ou URLs vers un dépôt git public pour consultation du code

## Explications techniques

### Annuaires

#### X.500

#### LDAP

### Schéma SUPANN2009

### Authentification et SSO

#### Kerberos (?)

#### SAML

#### OAuth

#### OIDC
