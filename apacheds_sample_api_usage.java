// From the official ApacheDS documentation
// Get the SchemaManager, we need it for this addition
SchemaManager schemaManager = directoryService.getSchemaManager();

// Create the partition
JdbmPartition sevenseasPartition = new JdbmPartition( schemaManager );
sevenseasPartition.setId("sevenseas");
Dn suffixDn = new Dn( schemaManager, "o=sevenseas" );
sevenseasPartition.setSuffix( suffixDn );
sevenseasPartition.setCacheSize(1000);
sevenseasPartition.init(directoryService);
sevenseasPartition.setPartitionPath( <a path on your disk> );

// Create some indices (optional)
sevenseasPartition.addindex( new JdbmIndex( "objectClass", false ) );
sevenseasPartition.addindex( new JdbmIndex( "o", false ) );

// Initialize the partition
sevenseasPartition.initialize();

// create the context entry
Entry contextEntry = new DefaultEntry( schemaManager, "o=sevenseas",
    "objectClass: top", 
    "objectClass: organization",
    "o: sevenseas" );

// add the context entry
sevenseasPartition.add( new AddOperationContext( null, entry ) );

// We are done !

