#!/bin/sh

curl -H "Content-type: application/json" \
     -H "Accept: application/json" \
     -d@traitement_submit005.json \
     http://forms-condorcet.dev.entrouvert.org/api/formdefs/invitation/submit
